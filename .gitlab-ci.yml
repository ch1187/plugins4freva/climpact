# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: CC0-1.0

stages:
  - lint
  - test
  - docs

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip

lint:
  stage: lint
  tags:
    - conda
  before_script:
      - apt-get update && apt-get install -y make
      - pip install .[testsite]
  script:
      - make lint


.config_freva_solr_mysql: &config_freva_solr_mysql
  image: ghcr.io/freva-clint/freva-dev:latest
  tags:
    - docker-any-image
  before_script:
    - export SCRATCH_DIR=/tmp
    - conda install --override-channels --channel conda-forge curl -y
    - curl -Ls https://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj bin/micromamba
    - export MAMBA_ROOT_PREFIX="$CI_PROJECT_DIR/.micromamba-$(echo $CI_JOB_NAME | tr '/' '-')"
    - eval "$(bin/micromamba shell hook --shell bash)"
    - echo 'eval "$(bin/micromamba shell hook --shell bash)"' >> ~/.bashrc
    - make micromamba-dep-install
    - micromamba activate ./plugin_env
    - micromamba install --override-channels --channel conda-forge -y python=3.12 pandoc graphviz wget zip git git-lfs jq freva mysql-common=8.3.0 mysql-libs=8.3.0
    - pip install .[testsite]
    - pip install pytest
    - git lfs install
    # ensure freva is up
    - freva --version
    # configure the freva plugin
    - export EVALUATION_SYSTEM_CONFIG_FILE=$CI_PROJECT_DIR/plugin_env/freva/evaluation_system.conf
    - export EVALUATION_SYSTEM_CONFIG_DIR=$CI_PROJECT_DIR/plugin_env/freva/

func-test:
  <<: *config_freva_solr_mysql
  stage: test
  script:
    - wget https://freva.gitlab-pages.dkrz.de/metadata-crawler-source/binaries/data-crawler -O data-crawler
    - chmod +x data-crawler
    - mkdir -p /home/freva/work
    - cd /home/freva
    - tar -xJvf $CI_PROJECT_DIR/data/work_nukleus.tar.xz work/
    - cd $CI_PROJECT_DIR
    - make prepare-freva
    - ./data-crawler solr add -ds nukleus-kit
    - export EVALUATION_SYSTEM_PLUGINS=$CI_PROJECT_DIR,climpact-wrapper-api
    - freva-plugin --list
    # TODO: This command has to be replaced with a freva cmd test in tests
    - freva-plugin climpact region=Garmisch-Partenkirchen project=nukleus product=ceu-3 experiment=historical models=miroc-miroc6-clmcom-kit-cclm-6-0-clm2-nukleus-x2yn2-v1 time_frequency=1hr variable=pr output_units=true output_file_type=nc mask_type=none mask_method=centres plot_map=true pre_process_function=pre_proc
    - mkdir -p build/func_test
    - FREVA_JOB_ID=$(ls plugin_env/freva_output/freva/freva/output/climpact/ | head -n 1)
    - cd plugin_env/freva_output/freva/freva/output/climpact/$FREVA_JOB_ID
    - mv 01A_Information.html index.html
    - echo '<html><body>' >> index.html
    - for IMAGE_FILE in MapPlot_*.png; do
        echo "<img src=\"$IMAGE_FILE\" alt=\"plot\" width=\"40%\"/>" >> index.html;
      done
    - echo '</body></html>' >> index.html
    - ls -la
    - cp -r * $CI_PROJECT_DIR/build/func_test
  artifacts:
    when: always
    paths:
      - build/func_test

prod-test:
  id_tokens:
    SITE_ID_TOKEN:
      aud: https://gitlab.dkrz.de
  tags:
    - levante
  variables:
    #####################################################
    # The following parameters are for the scheduler
    # If you need to change the scheduler parameters,
    # to fit your needs please change the following
    #####################################################
    SCHEDULER_PARAMETERS: "--account=$SLURM_ACCOUNT --partition=compute --mem=235G "
  script:
    #####################################################
    # This is how you can run your plugin in the production
    # environment.
    # REPLACE THE FOLLOWING LINES WITH YOUR OWN CONFIGURATION
    #####################################################
    - module load clint regiklim-ces
    - curl -Ls https://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj bin/micromamba
    - eval "$(bin/micromamba shell hook --shell bash)"
    - make micromamba-dep-install
    - micromamba activate ./plugin_env
    - ./plugin_env/bin/python -m pip install .
    - export EVALUATION_SYSTEM_PLUGINS=$CI_PROJECT_DIR,climpact-wrapper-api
    - freva-plugin --list
    - freva-plugin climpact region=Garmisch-Partenkirchen project=nukleus product=ceu-3 experiment=historical models=miroc-miroc6-clmcom-kit-cclm-6-0-clm2-nukleus-x2yn2-v1 time_frequency=1hr variable=pr output_units=true output_file_type=nc mask_type=none mask_method=centres plot_map=true pre_process_function=pre_proc caption="Climpact production test from CI piepline $CI_PROJECT_URL/-/jobs/$CI_JOB_ID"
    #####################################################

    - export PROJECT_DIR=/work/$SLURM_ACCOUNT/regiklim-work/$GITLAB_USER_LOGIN/regiklim-ces/output/climpact # CHANGE THIS LINE TO YOUR OWN PROJECT DIRECTORY
    - FREVA_JOB_ID=$(ls -t $PROJECT_DIR | head -n 1) # DON'T CHANGE THIS LINE
    - export JOB_URL=https://www-regiklim.dkrz.de/history/$FREVA_JOB_ID/results/ # CHANGE THIS LINE TO YOUR OWN PROJECT URL
    #####################################################
    # PLEASE DON'T CHANGE THE FOLLOWING LINES
    #
    # This part is basically designed to redirect the
    # user to the Freva evaluation system.
    #####################################################
    - mkdir -p build/prod_test
    - cd $PROJECT_DIR/$FREVA_JOB_ID
    - echo "<!DOCTYPE html><html><head>" >> index.html
    - echo "<meta http-equiv=\"refresh\" content=\"0; url=$JOB_URL\">" >> index.html
    - echo "<title>Redirecting...</title>" >> index.html
    - echo "</head><body>" >> index.html
    - echo "<p>If you are not redirected, <a href=\"$JOB_URL\">click here</a>.</p>" >> index.html
    - echo "</body></html>" >> index.html
    - cp -r * $CI_PROJECT_DIR/build/prod_test
  artifacts:
    when: always
    paths:
      - build/prod_test

unit-test:
  <<: *config_freva_solr_mysql
  stage: test
  script:
    - make test
    - make coverage
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    when: always
    paths:
      - htmlcov

test-docs:
  stage: docs
  tags:
    - conda
  before_script:
      - conda install --override-channels --channel conda-forge curl make -y
      - curl -Ls https://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj bin/micromamba
      - export MAMBA_ROOT_PREFIX="$CI_PROJECT_DIR/.micromamba-docs"
      - eval "$(bin/micromamba shell hook --shell bash)"
      - echo 'eval "$(bin/micromamba shell hook --shell bash)"' >> ~/.bashrc
      - make micromamba-dep-install
      - micromamba activate ./plugin_env
      - pip install sphinx
      - pip install -r docs/requirements.txt
      - pip install zstandard setuptools
      - pip install .[docs]
  script:
    # - make dev-install
    - make -C docs html
    - make -C docs linkcheck
  artifacts:
    paths:
    - docs/_build

pages:
  stage: docs
  tags:
    - sphinx
  needs:
    - test-docs
    - func-test
    - prod-test
    - unit-test
  script:
    - mkdir -p public/docs
    - cp -r docs/_build/html/* public/docs/
    - mkdir -p public/func_test
    - cp -r build/func_test/* public/func_test/
    - mkdir -p public/htmlcov
    - cp -r htmlcov/* public/htmlcov/
    - mkdir -p public/prod_test
    - cp -r build/prod_test/* public/prod_test/
  artifacts:
    paths:
    - public
  only:
  - main
  - master
