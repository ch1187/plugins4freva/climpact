<!--
SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum

SPDX-License-Identifier: CC0-1.0
-->

# Changelog

## v0.1.0: Initial release
