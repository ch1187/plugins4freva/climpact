# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: CC0-1.0

authors:
  # list author information. see
  # https://github.com/citation-file-format/citation-file-format/blob/main/schema-guide.md#definitionsperson
  # for metadata
  - family-names: "Bergemann"
    given-names: "Martin"
    affiliation: "Deutsche Klimarechenzentrum"
    # orcid: null
    email: "bergemann@dkrz.de"
cff-version: 1.2.0
message: "If you use this software, please cite it using these metadata."
title: "climpact"
license: 'BSD-3-Clause'
repository-code: "https://gitlab.dkrz.de/ch1187/plugins4freva/climpact"
url: "https://gitlab.dkrz.de/ch1187/plugins4freva/climpact"
contact:
  # list maintainer information. see
  # https://github.com/citation-file-format/citation-file-format/blob/main/schema-guide.md#definitionsperson
  # for metadata
  - family-names: "Hadizadeh"
    given-names: "Mostafa"
    affiliation: "Deutsche Klimarechenzentrum"
    # orcid: null
    email: "hadizadeh@dkrz.de"
  - family-names: "Wentzel"
    given-names: "Bianca"
    affiliation: "Deutsche Klimarechenzentrum"
    # orcid: null
    email: "wentzel@dkrz.de"
abstract: |
  Climpact is a freva plugin that processes climate model output data and creates to be used by climate impact model simulations. In it's default form the plugin creats field averages across a user defined region and saves the ouptut to one of the following file formats: 1.netcdf4 2.csv (comma seperated values - text format) 3.hdf5
