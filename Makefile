# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: CC0-1.0

.PHONY: clean clean-build clean-pyc clean-test coverage dist docs help install lint lint/flake8 lint/black
.DEFAULT_GOAL := help
# ANSI color codes
RED := \033[31m
GREEN := \033[32m
YELLOW := \033[33m
BLUE := \033[34m
BOLD := \033[1m
RESET := \033[0m

define BROWSER_PYSCRIPT
import os, webbrowser, sys

from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("\033[32m%-20s\033[0m %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

BROWSER := python -c "$$BROWSER_PYSCRIPT"

help:
	@echo "${BOLD}Available commands:${RESET}"
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test clean-venv ## remove all build, virtual environments, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -rf {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

clean-venv:  # remove the virtual environment
	rm -rf venv

lint/isort: ## check style with flake8
	isort --check src/climpact src/tests
lint/flake8: ## check style with flake8
	flake8 src/climpact src/tests
lint/black: ## check style with black
	black --check src/climpact src/tests
	blackdoc --check src/climpact src/tests
lint/reuse:  ## check licenses
	reuse lint

lint: lint/isort lint/black lint/flake8  lint/reuse ## check style

formatting:
	isort src/climpact src/tests
	black src/climpact src/tests
	blackdoc src/climpact src/tests

quick-test: ## run tests quickly with the default Python
	python -m pytest

test: ## run tox
	tox

test-all: test test-docs ## run tests and test the docs

coverage: ## check code coverage quickly with the default Python
	python -m pytest --cov src/climpact --cov-report=html
	$(BROWSER) htmlcov/index.html

docs: ## generate Sphinx HTML documentation, including API docs
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

test-docs: ## generate Sphinx HTML documentation, including API docs
	$(MAKE) -C docs clean
	$(MAKE) -C docs linkcheck

servedocs: docs ## compile the docs watching for changes
	watchmedo shell-command -p '*.rst' -c '$(MAKE) -C docs html' -R -D .

release: dist ## package and upload a release
	twine upload dist/*

dist: clean ## builds source and wheel package
	python -m build
	ls -l dist

install: clean ## install the package to the active Python's site-packages
	python -m pip install .

dev-install: clean ## install the package and development section
	python -m pip install -r docs/requirements.txt
	python -m pip install -e .[dev]

venv-install: clean  ## install the package in a virtual environment
	python -m venv venv
	venv/bin/python -m pip install -r docs/requirements.txt
	venv/bin/python -m pip install -e .[dev]
	venv/bin/pre-commit install

prepare-freva: clean ## prepare the configuration files for FREVA
	pytest src/tests/test_config_updates.py::test_prepare_freva -s

conda-dep-install: clean ## install a conda environment so-called plugin_env
	conda init bash
	conda env create --prefix ./plugin_env -f climpact-env.yml
mamba-dep-install: clean ## install a conda environment so-called plugin_env
	mamba env create --prefix ./plugin_env -f climpact-env.yml
micromamba-dep-install: clean ## install a conda environment so-called plugin_env
	bin/micromamba env create --override-channels --channel conda-forge --prefix ./plugin_env -f climpact-env.yml -y
# Important: Makefile cannot activate the conda environment. We have to do it manually.
dep-remove: clean ## remove the conda plugin_env environment
	conda env remove --name ./plugin_env
dep-update: clean ## update the conda plugin_env environment
	conda env update --prefix ./plugin_env -f climpact-env.yml

cartopy:
	# Use this if cartopy was installed and additional maps are downloaded.
	$(eval DATA_DIR =  $(shell plugin_env/bin/python -c 'from cartopy import config;print(config["repo_data_dir"])'))
	./plugin_env/bin/cartopy_feature_download.py gshhs physical cultural \
		cultural-extra -o $(DATA_DIR) --no-warn --ignore-repo-data
