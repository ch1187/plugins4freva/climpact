# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause
"""Apply the actual pre-processing."""
import base64
import json
import logging
import os
import sys
from concurrent.futures import (
    ProcessPoolExecutor,
    ThreadPoolExecutor,
    as_completed,
)
from datetime import datetime
from functools import partial
from getpass import getuser
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Callable, Dict, List, Optional, Tuple, Union

import dask

# import geopandas as gp
import humanize
import numpy as np
import pandas as pd
import papermill as pm

# import pyproj
# import regionmask
import xarray as xr

# from cartopy import crs
from ipykernel.kernelspec import install as install_kernel

from climpact import RunDirectory, Swift, UnitConverter
from climpact.utils import save_file

# from zipfile import ZipFile


logging.basicConfig(
    level=logging.INFO, format="%(name)s-%(levelname)s - %(message)s"
)
logger = logging.getLogger("ClimPact")


def parse_config(argv: List[str] = []) -> Path:
    """Construct command line argument parser."""
    import argparse

    argp = argparse.ArgumentParser
    ap = argp(
        prog="climate_change_profiler",
        description="Plot some maps of different climatechange profiles",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    ap.add_argument(
        "configfile",
        metavar="configfile",
        type=Path,
        help="The configuration file.",
    )
    args = ap.parse_args(argv)
    config_file = args.configfile.expanduser().absolute()
    with config_file.open() as f:
        config = json.load(f)
    # Destroy the potentially sensitive information in the config
    with config_file.open("w") as f:
        json.dump({}, f)
    return config


def install_jupyter_kernel(kernel_name: str) -> None:
    """Install a jupyter python kernel into the userspace."""

    install_kernel(
        user=True, kernel_name=kernel_name, display_name=kernel_name
    )


def get_datetime(
    da: Union[np.array, xr.DataArray], time: Union[list, np.array], **kwargs
) -> tuple:
    """Match a list time timestamps to the timeobject in a given data array.

    Parameters:
    ===========
    da : numpy.array
        time array as xarray.DataArray that contains the target time object
    time: collection
        collection of datetime objects that will be matched
    kwargs:
        Additional keyword arguments that can be used to overwrite,
        certain timestamp attributes e.g. year=2020
    Returns:
    ========
     list: list of timestamps with of the same type as da
    """
    try:
        da = pd.DatetimeIndex(da)
    except Exception:
        pass
    typ = np.asarray(da)[0]
    if isinstance(typ, np.datetime64):
        time = [datetime.utcfromtimestamp(t) for t in time.astype("O") / 1e9]
        typ = datetime
    else:
        typ = type(typ)
    add_kwargs = {}
    try:
        add_kwargs["has_year_zero"] = typ.has_year_zero
    except AttributeError:
        pass
    return [
        typ(
            kwargs.get("year", None) or t.year,
            kwargs.get("month", None) or t.month,
            kwargs.get("day", None) or t.day,
            kwargs.get("hour", None) or t.hour,
            kwargs.get("minute", None) or t.minute,
            kwargs.get("second", None) or t.second,
            **add_kwargs,
        )
        for t in time
    ], repr(typ)


def get_regions(geodata, key, region):
    if region:  # Region already selected
        yield None, "all"
    elif key is None:  # No split key was given
        yield None, "all"
    elif geodata is None:
        yield None, "all"
    else:
        try:
            col = [c for c in geodata.columns if c.lower() == key.lower()][0]
        except IndexError as error:
            raise ValueError("split key not in geo data.") from error
        for idx in geodata.index:
            region = str(geodata.loc[idx][col]).replace(" ", "-")
            yield geodata.loc[geodata.index == idx], region.replace("_", "-")


def process_run(
    files: List[str],
    variables: List[str],
    mask: Optional[Tuple[str, int]] = None,
    abort_on_error: bool = True,
    plot_map: bool = True,
    **metadata,
) -> xr.Dataset:
    """Process a model run.

    Parameters:
    ==========
    files:
        Output file names belonging to this run.
    variables:
        Variable names that are processed.
    abort_on_error:
        Exit if something goes wrong while loading the dataset
    mask:
        Tuple of file to land-sea mask and which type to mask (0: land, 1: sea)
    plot_mape:
        Indicate whether a map of the data should be plotted
    **metadata:
        Additional information on that run.
    """

    out_dir = Path(metadata.pop("out_dir"))
    pre_proc = metadata.pop("pre_proc")
    units = metadata.pop("output_units", {})
    split_by = metadata.pop("split_by", None) or None
    rd = RunDirectory(files, variables, abort_on_error, mask, **metadata)
    if rd.dataset is None:
        raise ValueError("Reading data failed, check error log.")
    coords = rd.dataset.coords
    attrs = rd.dataset.attrs.copy()
    large_dset = rd.dataset[rd.variables]
    data_groups = {}
    for geodata, region in get_regions(rd.georefdata, split_by, rd.region):
        if geodata is None:
            mask = rd.mask
        else:
            geodata = geodata.to_crs(rd.proj)
            mask = rd.get_mask(
                large_dset[rd.variables[0]],
                rd.mask_method,
                shape_data=geodata,
            )
            geodata = geodata["geometry"]
        dset = large_dset * mask.data
        extent = rd.get_extent(dset[rd.variables[0]], shape_data=geodata)
        if extent:
            dset = dset.sel(**extent)
        dset.attrs = attrs
        data_to_save = pre_proc(dset)
        try:
            for var in "X", "Y":
                data_to_save[var] = coords[var]
                data_to_save = data_to_save.set_coords(var)
        except xr.core.merge.MergeError:
            pass
        data_to_save.attrs = dset.attrs = attrs
        for var in data_to_save.data_vars:
            if var in rd.variables:
                old_attrs = rd.dataset[var].attrs
                data_attrs = data_to_save[var].attrs
                old_attrs |= data_attrs
                data_to_save[var].attrs = old_attrs
            if var in units:
                if isinstance(units[var], str):
                    data_to_save[var] = UnitConverter.convert(
                        data_to_save[var], units[var]
                    )
                else:
                    data_to_save[var].data *= units[var]
                    data_to_save[var].attrs["units"] += f" * {units[var]}"
        for var in rd.variables:
            dset[var].attrs = rd.dataset[var].attrs
            if var in units:
                if isinstance(units[var], str):
                    dset[var] = UnitConverter.convert(dset[var], units[var])
                else:
                    dset[var].data *= units[var]
                    dset[var].attrs["units"] += f" * {units[var]}"
        data_to_save.attrs = attrs
        fn = f"{metadata['model']}_{metadata['exp']}_{metadata['time_frequency']}_{region}"
        data_to_save.attrs[
            "filename"
        ] = f"Time_series_{metadata['product']}_{fn}"
        data_to_save.attrs["ensemble_member"] = metadata["model"]
        data_to_save.attrs["region"] = region or metadata["region"]
        data_to_save.attrs["experiment"] = metadata["exp"]
        data_to_save.attrs["output_freqeuncy"] = metadata["time_frequency"]
        data_to_save.attrs["shape_file"] = metadata.get("shape_file", "")
        data_to_save.attrs["swift_group"] = metadata["swift_group"]
        # Map plotting smoke test if rquested
        if plot_map:
            rd.plot_maps(dset, out_dir / f"MapPlot_{fn}", shape_data=geodata)
        data_groups[region] = data_to_save
    return data_groups


def load_method(
    file_path: str, method_name: str
) -> Callable[xr.Dataset, xr.Dataset]:  # noqa: F821
    """Try to load a method for pre-processing."""

    file_path = file_path or Path(__file__).parent / "assets" / "preproc.py"
    method_name = method_name or "pre_proc"
    module_path = Path(file_path).expanduser().absolute()
    sys.path.insert(0, str(module_path.parent))
    module = __import__(f"{module_path.with_suffix('').name}")
    return getattr(module, method_name)


def create_notebook(inp: str, out_dir: str, kernel_name: str) -> str:
    """Create a jupyter-notebook."""
    nb_in = Path(__file__).parent / "assets" / "PostPorcessing.ipynb"
    nb_out = Path(out_dir) / nb_in.name
    try:
        pm.execute_notebook(
            str(nb_in),
            str(nb_out),
            cwd=out_dir,
            parameters={
                "data_file": inp,
            },
            progress_bar=False,
            kernel_name=kernel_name,
            log_output=True,
            prepare_only=False,
            stdout_file=sys.stdout,
            stderr_file=sys.stderr,
        )
    except Exception:
        logger.error(
            f"Notebook execution failed. Find the notebook in: {nb_out}"
        )
        raise
    return str(nb_out)


def write_info_html(
    nb_file: Path, url: str, hpc_system: str = "levante", within: str = ""
):
    """Write information to html file."""

    out_dir = nb_file.parent
    out_file = out_dir / "01A_Information.html"
    run_time_info = ""
    if within:
        run_time_info = (
            f"The climpact plugin was successfully applied within {within}"
        )
    link = (
        f"https://jupyterhub.dkrz.de/user/{getuser()}/"
        f"{hpc_system}-spawner-preset/lab/tree/{nb_file}"
    )
    href = f"""<html>
    <body>
    <h3>Your data is ready for download the swift cloud object store.</h3>
    <p>{run_time_info}<br>
    To download the data follow <a href="{url}" target=_blank>this link</a></p>
    <h3>Additional data processing</h3>
    <p>Most certainly more data processing is needed for your impact model.
       you can spawn a jupyter notebook server with the link below:<br>
     <a href="{link}" target=_blank>{nb_file}</a><br>
     This approach is recommended if additional adaptations are needed.
     The notebook also offers a mini tutorial on how to use the library herin.
     </p>
     </body>
     </html>"""
    with out_file.open("w") as f:
        f.write(href)


def apply(config: Dict[str, str], tmp_dir: str) -> None:
    """Main function that applies the pre-processing.

    Parameters:
    -----------
    config: dict
        dictionary holding the configuration to apply the data processing.
    tmp_dir: str
        temporary working directory
    """
    start_time = datetime.now()
    key = config.pop("swift_key", None)
    config["kernel_name"] = "climpact"
    install_jupyter_kernel(config["kernel_name"])
    config["output_dir"] = Path(config["output_dir"])
    config.setdefault("abort_on_error", False)
    config.setdefault("plot_map", True)
    config.setdefault("output_units", {})
    debug = config.get("debug", False)
    if debug:
        logger.setLevel(10)
    logger.debug(config)
    pre_proc = load_method(
        config["pre_process_module"], config["pre_process_function"]
    )
    # Define the metadata that is needed to process the model runs
    metadata = dict(
        mask_method=config["mask_method"],
        exp=config["experiment"],
        product=config["product"],
        project=config["project"],
        out_dir=str(config["output_dir"]),
        shape_file=config["shape_file"] or "",
        region=config["region"] or "",
        start=config["start"] or "",
        end=config["end"] or "",
        swift_group=config["account"],
        pre_proc=pre_proc,
        time_frequency=config["time_frequency"],
        output_units=config["output_units"],
        split_by=config.get("split_by", "") or "",
        parallel=debug is False,
    )
    # Read the netcdf-files in parallel using a multi-threading
    logger.info("Reading netcdf files")
    variables = config["variable"]
    # models = []
    results = []
    with ThreadPoolExecutor(thread_name_prefix="ClimPact") as tp:
        futures = []
        for model, ensembles in config.pop("files").items():
            for ensemble, files in ensembles.items():
                if not files:
                    logger.critical(
                        "No files found for %s", f"{model}_{ensemble}"
                    )
                metadata["model"] = f"{model}_{ensemble}"
                proc_func = partial(
                    process_run,
                    sorted(files),
                    variables,
                    config["mask"] or None,
                    config["abort_on_error"],
                    config["plot_map"],
                    **metadata.copy(),
                )
                if debug is False:
                    futures.append(tp.submit(proc_func))
                else:
                    results.append(proc_func())
        if debug is False:
            results = [f.result() for f in as_completed(futures)]
        total = 0
        for res in results:
            total += len(res)
        if not results:
            raise ValueError("No files found")
        now = datetime.now().strftime("%Y%m%d_%H%M%ST")
        var = "_".join(variables)
        file_name = (
            f"ProcessedModelData-{config['project']}-{config['product']}-{var}"
        )
        username = getuser()

        logger.info(f"Writing time-series data - {total} files")
        if config["output_file_type"] == "zarr":
            data_file = Path(tmp_dir) / username / f"{file_name}-{now}.zarr"
        else:
            data_file = Path(tmp_dir) / username / f"{file_name}-{now}.zip"
        data_file.parent.mkdir(exist_ok=True, parents=True)
        with dask.config.set(pool=ProcessPoolExecutor()):
            for nn, groups in enumerate(results):
                for region, result in groups.items():
                    result.attrs["path_prefix"] = file_name + region
                    results[nn][region] = result.compute(scheduler="processes")
        with save_file(data_file, "w") as s_file:
            nn = 1
            for groups in results:
                for region, result in groups.items():
                    logger.info(f"{nn} of {total} saved ({nn/total:02.01%})")
                    RunDirectory.save_data(
                        s_file, result, config["output_file_type"]
                    )
                    nn += 1
        logger.info("Uploading data to swift object store")
        try:
            passwd = base64.b64decode(key).decode()
        except TypeError:
            passwd = None
        passwd = passwd or None
        swift = Swift(config["account"], username=username, password=passwd)
        url = swift.upload(data_file.parent, "climpact")
    nb_file = create_notebook(
        f"{url}/{data_file.name}",
        str(config["output_dir"]),
        config["kernel_name"],
    )
    for file in config["output_dir"].rglob("*"):
        file.chmod(0o755)
    config["output_dir"].chmod(0o755)
    nb_file = Path(nb_file)
    within = humanize.naturaldelta(datetime.now() - start_time)
    write_info_html(nb_file, f"{url}/{data_file.name}", within=within)
    logger.info(f"A notebook for post-processing is ready at {nb_file}")
    logger.info(
        f"Data was uploaded. To access the data use the url: {url}/{data_file.name}"
    )


if __name__ == "__main__":
    username = getuser()
    scratch = os.getenv("SCRATCH_DIR", f"/scratch/{getuser()[0]}/{getuser()}")
    with TemporaryDirectory(dir=scratch) as td:
        cfg = apply(parse_config(sys.argv[1:]), td)
