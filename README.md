<!--
SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum

SPDX-License-Identifier: CC-BY-4.0
-->

# ClImpact

[![CI](https://gitlab.dkrz.de/ch1187/plugins4freva/climpact/badges/main/pipeline.svg)](https://gitlab.dkrz.de/ch1187/plugins4freva/climpact/-/pipelines?page=1&scope=all&ref=main)
[![Code coverage](https://gitlab.dkrz.de/ch1187/plugins4freva/climpact/badges/main/coverage.svg)](https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/htmlcov/)
[![Docs](https://img.shields.io/badge/docs-Passed-green.svg)](https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/docs/)
[![Functional](https://img.shields.io/badge/Functional-Test-brown.svg)](https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/func_test/)
[![Production](https://img.shields.io/badge/Production-Test-ff33fc.svg)](https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/prod_test/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
[![REUSE status](https://api.reuse.software/badge/gitlab.dkrz.de/ch1187/plugins4freva/climpact)](https://api.reuse.software/info/gitlab.dkrz.de/ch1187/plugins4freva/climpact)


**Climpact** is a [freva plugin](https://www-regiklim.dkrz.de/>) that processes
climate model output data and creates to be used by climate impact model simulations.
In it's default form the plugin creats field averages across a user defined region
and saves the ouptut to one of the following file formats:

- netcdf4
- csv (comma seperated values - text format)
- hdf5


## Connection and Module Loading

To connect to the server and load the necessary modules for the project, use the following commands:

```bash
ssh <user-id>@levante.dkrz.de
module load clint <project-module>
```
whcih in RegKlim project, `<project-module>` is `regiklim-ces`

## Integrating climpact with the Plugin

To integrate climpact into your Freva instance, follow these steps:

1. **Clone the Plugin Repository:**

    Navigate to the appropriate directory and clone the repository using the following commands:

    ```bash
    cd <project-dir>
    git clone https://gitlab.dkrz.de/ch1187/plugins4freva/climpact.git
    ```
    in RegiKlim, `<project-dir>` is set to `/work/ch1187/regiklim-work/<username>/plugins/`

2. **Install Prerequisites:**

    After cloning the repository, install the required packages:

    ```bash
    cd climpact
    make dep-install
    conda activate plugin_env
    ```

    or simply the following conda command:

    ```bash
    cd climpact
    conda env create --prefix ./plugin_env -f climpact-env.yml
    conda activate plugin_env
    pip install .
    ```

3. **Configure the Freva Instance:**

    Use the following export command to make the plugin available in your Freva instance:

    ```bash
    export EVALUATION_SYSTEM_PLUGINS=<project-dir>/climpact,climpact-wrapper-api
    ```
    For example, in RegiKlim, `<project-dir>` is set to `/work/ch1187/regiklim-work/<username>/plugins/`. You
    can adjust this path based on the specific project you want to plug it into.

Now, the plugin is ready to be used in your Freva instance.

## Development and contribution

To use this in a development setup, clone the [source code][source code] from
gitlab, start the development server and make your changes:

```bash
git clone https://gitlab.dkrz.de/ch1187/plugins4freva/climpact.git
cd climpact
conda env create --prefix ./plugin_env -f climpact-env.yml
conda activate plugin_env
```

More detailed installation instructions my be found in the [docs][docs].


[source code]: https://gitlab.dkrz.de/ch1187/plugins4freva/climpact


For more information on how to contribute to the plugin as well as important details regarding standars and license management chek the [contributing guide][contributing].


### Forking and Setting Up CI/CD for This Plugin

If you plan to fork this plugin and develop it within your desired DKRZ gitlab group while ensuring that `func-test` and `prod-test` run smoothly, you must first add the following variables to your CI/CD environment:

- **CI_SWIFT_USER**: Your username on Swift Cloud  
- **CI_SWIFT_PASSWD**: Your password on Swift Cloud  
- **CI_SWIFT_ACCOUNT**: The group account associated with your Swift Cloud username  
- **SLURM_ACCOUNT**: The Slurm account you want to use for job submissions  

By setting these variables correctly, you can successfully execute both functional and production tests in your CI/CD pipeline.

### Contributing Guidelines

Before pushing your changes, please ensure that all **mypy** and linting checks pass successfully by running:  

```sh
pip install .[testsite]
make test
```

Additionally, to pass **pytest** locally, you need to have a **Freva** instance running on your machine! If you encounter pytest errors, to facilitate the development process, you can also check the CI pipeline, which provides a running **Freva** instance for testing, and bug fix there.



## Technical note

This package has been generated from the template
[Freva Plugin Template](https://gitlab.dkrz.de/ch1187/plugins4freva/freva-plugin-template).

See the template repository for instructions on how to update the skeleton for
this package.


## License information

Copyright © 2024 Deutsche Klimarechenzentrum



Code files in this repository are licensed under the
BSD-3-Clause, if not stated otherwise
in the file.

Documentation files in this repository are licensed under CC-BY-4.0, if not stated otherwise in the file.


Supplementary and configuration files in this repository are licensed
under CC0-1.0, if not stated otherwise
in the file.

Please check the header of the individual files for more detailed
information.



### License management

License management is handled with [``reuse``](https://reuse.readthedocs.io/).
If you have any questions on this, please have a look into the
[contributing guide][contributing] or contact the maintainers of
`climpact`.
