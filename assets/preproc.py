# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

import xarray as xr


def pre_proc(dset: xr.Dataset, **kwargs) -> xr.Dataset:
    """Create a simple field mean of a given dataset."""

    dims = [d for d in dset.dims.keys() if "lat" in d or "lon" in d]
    return dset.mean(dim=dims)
