# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause


"""
Climpact plugin, that processes climate model output to impact model input.
"""
import json
import logging
import os
from getpass import getuser
from pathlib import Path
from tempfile import NamedTemporaryFile

import freva
from assets.climpact_assets import MUNICIPALITIES
from evaluation_system.api import plugin
from evaluation_system.api.parameters import Bool, File
from evaluation_system.api.parameters import ParameterDictionary as ParamDict
from evaluation_system.api.parameters import SelectField, SolrField, String
from evaluation_system.misc import config, logger
from evaluation_system.model.solr import SolrFindFiles


class Climpact(plugin.PluginAbstract):
    """Setup the config for climate model output data."""

    tool_developer = {"name": "Martin Bergemann", "email": "bergemann@dkrz.de"}
    __category__ = "support"
    __short_description__ = (
        "Process climate model data for input of impact model"
    )
    __long_description__ = (
        "This plugin processes multi dimensional climate "
        "model data. The output of this plugin can "
        "be used for various impact model simulations. "
        "The plugin selects a region of interest "
        "as defined by a geojson file and calculates "
        "a time-series of area weighted field means over the "
        "defined region."
        "defined region. NOTE: to turn on debug mode use the --debug or "
        " -vvv flag on the freva command line interface."
    )
    __version__ = (2024, 8, 20)
    __parameters__ = ParamDict(
        File(
            name="shape_file",
            file_extension="geojson",
            help=(
                "Select a geo reference file defining "
                "your region of interest, if None is selected (default), "
                "the whole region, that is defined in the climate data "
                "will be taken. Note: You can either select a path on "
                "on the HPC system, or a web url pointing to the file."
            ),
            default=None,
        ),
        SelectField(
            name="region",
            options={mun: mun for mun in [""] + sorted(MUNICIPALITIES)},
            help=(
                "Select a pre defined German municipality of interest. "
                "This selection has only effect if you don't chose a shape file."
            ),
        ),
        String(
            name="split_by",
            help=(
                "If your selected geo reference file has multiple geometries "
                "and you whish to process each geometry separatly you can "
                "choose a split key that to split the geometries into different "
                "sub regions. The values for the split key will thereby used to "
                "distinguish the sub regions."
            ),
        ),
        SolrField(
            name="project",
            mandatory=False,
            facet="project",
            help=(
                "Select the climate parent project, to refine the model search"
            ),
            max_items=1,
            predefined_facets={"project": ["cordex", "nukleus"]},
        ),
        SolrField(
            name="product",
            mandatory=False,
            facet="product",
            help=(
                "Select the climate parent product, to refine the model search"
            ),
            max_items=1,
        ),
        SolrField(
            name="experiment",
            mandatory=True,
            facet="experiment",
            help="Choose a climate model experiment.",
            max_items=1,
            predefined_facets=dict(project=["cordex", "nukleus"]),
        ),
        SolrField(
            name="models",
            mandatory=True,
            facet="model",
            multiple=True,
            max_items=100,
            predefined_facets=dict(project=["cordex", "nukleus"]),
            help=(
                "Select the climate model(s). "
                "You can select multiple models, to get output for "
                "multiple models at once."
            ),
        ),
        SolrField(
            name="time_frequency",
            mandatory=True,
            facet="time_frequency",
            multiple=False,
            default="day",
            help="Select the climate model output time frequency.",
        ),
        SolrField(
            name="variable",
            mandatory=True,
            facet="variable",
            multiple=True,
            max_items=100,
            help="Set the variable name(s) of interest.",
        ),
        String(
            name="start",
            help=(
                "Set the first timestep of the climate data that should be "
                "processed. If empty (default) the first time step of "
                "climate data is taken."
            ),
            default=None,
        ),
        String(
            name="end",
            help=(
                "Set the last timestep of the climate data that should be "
                "processed. If empty (default) the last time step of "
                "climate data is taken."
            ),
            default=None,
        ),
        String(
            name="output_units",
            default="",
            help=(
                "If you want to change the output units you can set a string "
                "representing a lookup table of units or multiplyer "
                "for example if you whish to convert percipitation (pr) from "
                "its standard unit to mm/h you can define the following lookup "
                "table: `pr:mm/h` or `pr:3600`. In the first case the code "
                "will try to convert input precipitation unit to `mm/h` in the "
                "latter case the precipitation will be multiplied by 3600. "
                "You can pass multiple values separated by a `,`: "
                "For example pr:3600, tas:degC. "
            ),
        ),
        SelectField(
            name="output_file_type",
            default="nc",
            options={"nc": "nc", "csv": "csv", "h5": "hdf5", "zarr": "zarr"},
            help="Select the filetype of the plugin output.",
        ),
        SelectField(
            name="mask_type",
            default="none",
            options={"none": "none", "land": "land", "sea": "sea"},
            help=(
                "Surface type that should be MASKED (none: no mask is applied "
                "land: all land areas are masked, sea: all water areas are masked"
            ),
        ),
        SelectField(
            name="mask_method",
            default="centres",
            options={"centres": "centres", "corners": "corners"},
            help=(
                "How the masked region is determined. `centres` selects points "
                "where only gird-cell centres are within the polygone of the shape "
                "region. `corners` selects grid-cells where any of the 4 gird-cell "
                "corners lie within the polygone of the shape region, select "
                "`corners` if you want to be the region selection less restrictive."
            ),
        ),
        Bool(
            name="plot_map",
            default=True,
            help=(
                "Plot a map of each selected variable and model to check "
                "the projection and masking of the region."
            ),
        ),
        File(
            name="pre_process_module",
            file_extension="py",
            help=(
                "If you whish to apply your own processing routine "
                "set the path to the python file where the routine routine is defined. "
                "If not set (default) a time series of the selected variables "
                "will be calculated."
            ),
            default=None,
        ),
        String(
            name="pre_process_function",
            help=(
                "Set the name of the function defining the pre processing "
                "routine. This option comes only into affect if the "
                "`pre_process_module` option is set."
            ),
            default="pre_proc",
        ),
    )

    @staticmethod
    def _drs_search(**search_kw):
        kw = search_kw.copy()
        files = sorted(freva.databrowser(**kw))
        if files:
            return files

    def _get_files(
        self, variable, experiment, model, product, project, freq, start, end
    ):
        files = {}
        time = {}
        if start and end:
            time = {"time": f"{start}to{end}"}
        elif start:
            time = {"time": f"{start}to9999"}
        elif end:
            time = {"time": f"0to{end}"}

        search_kw = dict(
            variable=variable,
            experiment=experiment,
            model=model,
            time_frequency=freq,
        )
        search_kw.update(time)
        if product:
            search_kw["product"] = product
        if project:
            search_kw["project"] = project
        experiment_facets = freva.facet_search(facet=["ensemble"], **search_kw)
        for ens in experiment_facets["ensemble"]:
            search_kw["ensemble"] = ens
            files_found = self._drs_search(**search_kw)
            if not files_found:
                logger.warning("No files found for %s", ens)
            else:
                files[ens] = files_found
        return files

    def get_mask(self, mask_type, **kwargs):
        """Get the land-sea mask for a setup."""
        setup = kwargs.copy()
        if mask_type is None or mask_type == "none":
            return
        setup["variable"] = "sftlf"
        files = self._drs_search(**setup)
        if files:
            return str(files[0]), mask_type
        logger.warning(
            "No land-sea mask found for setup, searching for alternatives"
        )
        # Get rid of the model contraint
        setup.pop("model")
        files = self._drs_search(**setup)
        if files:
            return str(files[0]), mask_type
        raise ValueError("No land-sea mask found for setup")

    def run_tool(self, config_dict):
        out_dir = self._special_variables.substitute({"d": "$USER_OUTPUT_DIR"})
        out_dir = Path(out_dir["d"]) / str(self.rowid)
        debug = logger.getEffectiveLevel() <= logging.DEBUG
        config_dict["output_dir"] = str(out_dir)
        out_dir.mkdir(exist_ok=True, parents=True)
        models = config_dict.pop("models")
        units = {}
        for string in (
            config_dict.pop("output_units").strip("{").strip("}").split(",")
        ):
            var, _, unit = string.strip().partition(":")
            if var:
                try:
                    units[var.strip()] = float(unit.strip())
                except ValueError:
                    units[var.strip()] = unit.strip()
        config_dict["output_units"] = units
        files = {}
        search_kw = dict(
            model=models,
            experiment=config_dict["experiment"],
            variable=config_dict["variable"],
        )
        for key in ("project", "product"):
            if config_dict[key] is not None:
                search_kw[key] = config_dict[key]
        config_dict["mask"] = self.get_mask(
            config_dict.pop("mask_type"), **search_kw
        )
        if config_dict["project"] is None:
            config_dict["project"] = [
                project
                for project in SolrFindFiles.facets(**search_kw)["project"][
                    ::2
                ]
                if project
            ]
        if isinstance(models, str):
            models = [models]
        for model in models:
            files[model] = self._get_files(
                config_dict["variable"],
                config_dict["experiment"],
                model,
                config_dict["product"],
                config_dict["project"],
                config_dict["time_frequency"],
                config_dict["start"],
                config_dict["end"],
            )
        config_dict["swift_key"] = os.environ.get("LC_TELEPHONE", None)
        options = config.get_section("scheduler_options")
        config_dict["account"] = options.get("project") or getuser()
        config_dict["files"] = files
        config.reloadConfiguration()
        this_dir = Path(__file__).parent.absolute()
        config_dict["kernel_name"] = config.get("project_name", "freva")
        python_env = this_dir / "plugin_env" / "bin" / "python"
        tool_path = this_dir / "ProcessClimateData.py"
        if not config_dict["shape_file"]:
            config_dict["shape_file"] = str(this_dir / "assets" / "GER.shp")
            for num, mun in enumerate(MUNICIPALITIES):
                if mun.lower() == config_dict["region"].lower():
                    config_dict["region"] = str(num)
                    break
        else:
            config_dict["region"] = ""
        config_dict["debug"] = False
        for key, value in config_dict.items():
            if value is None:
                config_dict[key] = ""
        if debug:
            config_dict["abort_on_error"] = True
        config_dict["debug"] = debug
        with NamedTemporaryFile(suffix=".json") as tf:
            with open(tf.name, "w") as f:
                json.dump(config_dict, f, indent=3)
            cmd = "{} -B {} {}".format(python_env, tool_path, tf.name)
            _ = self.call(cmd)
        for path in ("dask-worker-space", "__pycache__"):
            rmpath = Path(config_dict["output_dir"]) / path
            if rmpath.is_dir():
                os.system("rm -r %s" % str(rmpath))
        return self.prepare_output(config_dict["output_dir"])
