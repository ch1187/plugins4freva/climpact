.. SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _api:

API Reference
=============


.. toctree::

    api/climpact
