# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from pathlib import Path

from sphinx.ext import apidoc

sys.path.insert(0, os.path.abspath(".."))

if not os.path.exists("_static"):
    os.makedirs("_static")

# isort: off
sys.path.insert(0, os.path.abspath("../src"))
import climpact

# isort: on


def generate_apidoc(app):
    appdir = Path(app.__file__).parent
    apidoc.main(
        ["-fMEeTo", str(api), str(appdir), str(appdir / "migrations" / "*")]
    )


api = Path("api")

if not api.exists():
    generate_apidoc(climpact)

# -- Plugin information -----------------------------------------------------

project = "climpact"

author = "Martin Bergemann"


linkcheck_ignore = [
    # we do not check link of the climpact as the
    # badges might not yet work everywhere. Once climpact
    # is settled, the following link should be removed
    r"https://.*climpact"
]


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "hereon_nc_sphinxext",
    "sphinx.ext.intersphinx",
    "sphinx_design",
    "sphinx.ext.napoleon",
    "sphinx.ext.todo",
    "autodocsumm",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "nbsphinx",
    "sphinx.ext.viewcode",
    "sphinxcontrib_github_alt",
    "sphinx.ext.coverage",
    "numpydoc",
]


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


autodoc_default_options = {
    "show_inheritance": True,
    "members": True,
    "autosummary": True,
}


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = "sphinx_rtd_theme"

html_theme_options = {
    "collapse_navigation": False,
    "includehidden": False,
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]


intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
    "django": ("https://django.readthedocs.io/en/stable/", None),
}

linkcheck_ignore = [
    r"data_shape.html",
    r"https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/docs/",
    r"https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/func_test/",
    r"https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/prod_test/",
    r"https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/htmlcov/",
    r"https://api.reuse.software/info/gitlab.dkrz.de/ch1187/plugins4freva/climpact",
]
