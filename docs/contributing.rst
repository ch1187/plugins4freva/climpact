.. SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _contributing:

Contribution and development hints
==================================

.. warning::

   This page has been automatically generated as has not yet been reviewed by the
   authors of climpact!

The climpact plugin is developed by the
`Deutsche Klimarechenzentrum`_. It is open-source
as we believe that this analysis can be helpful for reproducibility and
collaboration, and we are looking forward for your feedback,
questions and especially for your contributions.

- If you want to ask a question, are missing a feature or have comments on the
  docs, please `open an issue at the source code repository`_
- If you have suggestions for improvement, please let us know in an issue, or
  fork the repository and create a merge request. See also :ref:`development`.

.. _Deutsche Klimarechenzentrum: https://www.dkrz.de
.. _open an issue at the source code repository: https://gitlab.dkrz.de/ch1187/plugins4freva/climpact

.. _development:

Contributing in the development
-------------------------------

Installation
^^^^^^^^^^^^

Thanks for your wish to contribute to this plugin!! The source code of
the `climpact` package is hosted at
https://gitlab.dkrz.de/ch1187/plugins4freva/climpact.


Once you created an account in this gitlab, you can fork_ this
repository to your own user account and implement the changes.

Afterwards, please make a merge request into the main repository. If you
have any questions, please do not hesitate to create an issue on gitlab
and contact the maintainers of this package.

Once you created you fork, you can clone it via

.. code-block:: bash

    git clone https://gitlab.dkrz.de/<your-user>/climpact.git

we recommend that you change into the directory and create a virtual
environment via::

   cd climpact
   python -m venv venv
   source venv/bin/activate # (or venv/Scripts/Activate.bat on windows)

and install it in development mode with the ``[dev]`` option via::

    pip install -e ./climpact/[dev]


Development Helpers
-------

Shortcuts with make
^^^^^^^^^^^^^^^^^^^

There are several shortcuts available with the ``Makefile`` in the root of
the repository. On Linux, you can execute ``make help`` to get an overview.

Contributing to the code
------------------------

Format checking
^^^^^^^^^^^^^^^^

We use automated formatters to ensure a high quality and maintanability of
our source code (see their config in ``pyproject.toml``).

- `Black <https://black.readthedocs.io/en/stable/>`__ for standardized
  code formatting
- `blackdoc <https://blackdoc.readthedocs.io/en/latest/>`__ for
  standardized code formatting in documentation
- `Flake8 <http://flake8.pycqa.org/en/latest/>`__ for general code
  quality
- `isort <https://github.com/PyCQA/isort>`__ for standardized order in
  imports.
- `mypy <http://mypy-lang.org/>`__ for static type checking on
  `type hints <https://docs.python.org/3/library/typing.html>`__
- `reuse <https://reuse.readthedocs.io/>`__ for handling of licenses
- `cffconvert <https://github.com/citation-file-format/cff-converter-python>`__
  for validating the ``CITATION.cff`` file.

Formatting is integrated into the process of development via git hooks running
before each commit using `pre-commit <https://pre-commit.com/>`__.
We highly recommend that you setup pre-commit hooks following this guide::

  1. Run the following command to set up the git hooks:
   pre-commit install

  2. Automatic Checks:
   Now, each time you attempt to commit changes, the pre-commit hooks will
   automatically run to ensure your files adhere to the specified standards.

  3. Manual Execution:
   pre-commit run --all-files

You can skip the pre-commit checks with ``git commit --no-verify`` but note
that the CI will fail if it encounters any formatting errors.


Format
Annotating licenses
^^^^^^^^^^^^^^^^^^^

If you want to create new files, you need to set license and copyright
statements correctly. We use ``reuse`` to check that the licenses are
correctly encoded. As a helper script, you can use the script at
``.reuse/add_license.py`` that provides several shortcuts from
``.reuse/shortcuts.yaml``. Please select the correct shortcut, namely

- If you create a new python file, you should run::

      python .reuse/add_license.py code <file-you-created>.py

- If you created a new file for the docs, you should run::

      python .reuse/add_license.py docs <file-you-created>.py

- If you created any other non-code file, you should run::

      python .reuse/add_license.py supp <file-you-created>.py

If you have any questions on how licenses are handled, please do not hesitate
to contact the maintainers of `climpact`.


Fixing the docs
---------------
The documentation for this package is written in restructured Text and built
with sphinx_ and deployed on readthedocs_.

If you found something in the docs that you want to fix, head over to the
``docs`` folder, install the necessary requirements via
``pip install -r requirements.txt ../[docs]`` and build the docs with
``make html`` (or ``make.bat`` on windows).

The docs are then available in ``docs/_build/html/index.html`` that you can
open with your local browser.

Implement your fixes in the corresponding ``.rst``-file and push them to your
fork on gitlab.

.. _fork: https://gitlab.dkrz.de/ch1187/plugins4freva/climpact/-/forks/new

.. _sphinx: https://www.sphinx-doc.org
.. _readthedocs: https://readthedocs.org


Updating the skeleton for this package
--------------------------------------

This package has been generated from the template
`https://gitlab.dkrz.de/ch1187/plugins4freva/freva-plugin-template.git`__.

See the template repository for instructions on how to update the skeleton for
this package.
