.. SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
..
.. SPDX-License-Identifier: CC-BY-4.0

.. climpact documentation master file, created by
   sphinx-quickstart on Wed Sep 25 14:59:56 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Freva Plugin: Climate Model Output to Impact Model Input (climpact)
====================================

|CI|
|Code coverage|
|Production|
|Functional|
|Code style: black|
|Imports: isort|
|PEP8|
|Checked with mypy|
|REUSE status|

**Climpact** is a `freva plugin  <https://www-regiklim.dkrz.de/>`_ that processes
climate model output data and creates output that is meant to  be used by
any kind of impact model simulations. By default the plugin creats field
averages across a user defined region and saves the ouptut in one of the
following file formats:

* netcdf4
* csv (comma seperated values - text format)
* hdf5

Users can also specify their own pre processing routine if they don't want to
create time series data or want to calculate derived variables.

.. warning::

    This page has been automatically generated as has not yet been reviewed by
    the authors of climpact!
    Stay tuned for updates and discuss with us at
    https://gitlab.dkrz.de/ch1187/plugins4freva/climpact


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   plugin_setup
   standalone
   data_shape.ipynb
   post_process
   PostProcessingExample.ipynb
   FAQ.ipynb
   api
   contributing


How to cite this software
-------------------------

.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff


License information
-------------------

The source code of climpact is licensed under
BSD-3-Clause.

If not stated otherwise, the contents of this documentation is licensed under
CC-BY-4.0.


.. seealso::
    `Freva website <http://www-regiklim.dkrz.de>`_

    `Xarray <http://xarray.pydata.org/en/stable/>`_

    `Dask <https://docs.dask.org/en/latest/>`_


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. |CI| image:: https://gitlab.dkrz.de/ch1187/plugins4freva/climpact/badges/main/pipeline.svg
   :target: https://gitlab.dkrz.de/ch1187/plugins4freva/climpact/-/pipelines?page=1&scope=all&ref=main
.. |Code coverage| image:: https://gitlab.dkrz.de/ch1187/plugins4freva/climpact/badges/main/coverage.svg
   :target: https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/htmlcov/
.. |Code style: black| image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. |Imports: isort| image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://pycqa.github.io/isort/
.. |PEP8| image:: https://img.shields.io/badge/code%20style-pep8-orange.svg
   :target: https://www.python.org/dev/peps/pep-0008/
.. |Checked with mypy| image:: http://www.mypy-lang.org/static/mypy_badge.svg
   :target: http://mypy-lang.org/
.. |Functional| image:: https://img.shields.io/badge/Functional-Test-brown.svg
   :target: https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/func_test/
.. |Production| image:: https://img.shields.io/badge/Production-Test-ff33fc.svg
   :target: https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/prod_test/
.. |REUSE status| image:: https://api.reuse.software/badge/gitlab.dkrz.de/ch1187/plugins4freva/climpact
   :target: https://api.reuse.software/info/gitlab.dkrz.de/ch1187/plugins4freva/climpact
