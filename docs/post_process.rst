.. SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
..
.. SPDX-License-Identifier: CC-BY-4.0

Further processing
==================

For working with the ouput data of the plugin a special library offers
the creation of so called data-containers. These data-containers allow
for easy access of the data in the cloud. The plugin has also created a jupyter
notebook that demonstrates the usage of such data-containers.
It is very likely that the data hasn't been processed sufficiently by the plugin,
e.g unit conversion or calculating new variables is necessary.
This step, which is highly individual, can be done with help of this jupyter notebook.

The notebook assumes that the data has already been processed by the freva plugin
and was successfully stored in the cloud.



Creating a data container
-------------------------
Once the plugin output is saved to a cloud storage object the data-container
holding the plugin output data can be created by applying the :func:`from_zipfile`
method of the :func:`DataContainer` class

.. automodule:: climpact.datacontainer
   :members:
   :undoc-members:
