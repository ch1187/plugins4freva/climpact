.. SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
..
.. SPDX-License-Identifier: CC-BY-4.0


The climpact stand alone package
================================

.. toctree::
   :maxdepth: 2

.. automodule:: climpact

.. automodule:: climpact.RunDirectory
   :members:
   :show-inheritance:
