# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: CC0-1.0

[build-system]
build-backend = 'setuptools.build_meta'
requires = ['setuptools >= 61.0', 'versioneer[toml]']

[project]
name = "climpact"
dynamic = ["version"]
description = "Climpact is a freva plugin that processes climate model output data and creates to be used by climate impact model simulations. In it's default form the plugin creats field averages across a user defined region and saves the ouptut to one of the following file formats: 1.netcdf4 2.csv (comma seperated values - text format) 3.hdf5"

readme = "README.md"
authors = [
    { name = 'Martin Bergemann', email = 'bergemann@dkrz.de' },
]
maintainers = [
    { name = 'Mostafa Hadizadeh', email = 'hadizadeh@dkrz.de' },
    { name = 'Bianca Wentzel', email = 'wentzel@dkrz.de' },
]
license = { text = 'BSD-3-Clause' }

classifiers = [
    "Intended Audience :: Developers",
    "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
    "Operating System :: OS Independent",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Typing :: Typed",
]

requires-python = '>= 3.9'
dependencies = [
        "appdirs",
        "geopandas",
        "regionmask",
        "xarray",
        "cf_xarray",
        "cftime",
        "cartopy",
        "nc-time-axis",
        "h5netcdf",
        "pint",
        "pint-xarray",
        "python-swiftclient",
        "swiftspec",
        "zarr",
        "tqdm",
]

[project.urls]
Homepage = 'https://gitlab.dkrz.de/ch1187/plugins4freva/climpact'

Documentation = "https://ch1187.gitlab-pages.dkrz.de/plugins4freva/climpact/"

Source = "https://gitlab.dkrz.de/ch1187/plugins4freva/climpact"
Tracker = "https://gitlab.dkrz.de/ch1187/plugins4freva/climpact/issues/"


[project.optional-dependencies]
testsite = [
    "tox==4.17.1",
    "isort==5.12.0",
    "black==23.1.0",
    "blackdoc==0.3.8",
    "flake8==6.0.0",
    "pre-commit",
    "mypy",
    "pytest-cov",
    "reuse",
    "cffconvert",
    "types-requests",
    "toml",
]
docs = [
    "autodocsumm",
    "sphinx-rtd-theme",
    "sphinx-design",
    "myst_parser",
    "sphinx",
    "nbsphinx",
    "numpydoc",
    "recommonmark",
    "sphinx_rtd_theme",
    "ipython",  # For nbsphinx syntax highlighting
    "sphinxcontrib_github_alt",
    "pandoc"
]
dev = [
    "climpact[testsite]",
    "climpact[docs]",
    "PyYAML",
    "toml",
]

[tool.mypy]
ignore_missing_imports = true

[tool.setuptools]
zip-safe = false
license-files = ["LICENSES/*"]

[tool.setuptools.package-data]
climpact = ["py.typed"]

[tool.setuptools.packages.find]
namespaces = false
where = ["src"]
exclude = [
    'docs',
    'tests*',
    'examples'
]

[tool.pytest.ini_options]
addopts = '-v'

[tool.versioneer]
VCS = 'git'
style = 'pep440'
versionfile_source = 'src/climpact/_version.py'
versionfile_build = 'src/climpact/_version.py'
tag_prefix = 'v'
parentdir_prefix = 'climpact-'

[tool.isort]
profile = "black"
line_length = 79
src_paths = ["climpact"]
float_to_top = true
known_first_party = "climpact"

[tool.black]
line-length = 79
target-version = ['py39']

[tool.coverage.run]
omit = ["climpact/_version.py"]
