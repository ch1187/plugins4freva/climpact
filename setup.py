# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: CC0-1.0

"""Setup script for the climpact package."""
import versioneer
from setuptools import find_packages, setup

setup(
    version=versioneer.get_version(),
    packages=find_packages("src"),
    package_dir={"": "src"},
)
