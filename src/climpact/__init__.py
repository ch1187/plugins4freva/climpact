# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

""":py:mod:`climapct` is both a freva plugin and a stand alone library
that can be used to process climate data in order to select regions defined
by coordinates stored in geo reference data, like shape or geojson files.

Installation
------------

The best method to install the library is cloning the repository and creating
a conda environment:

::

    git clone https://gitlab.dkrz.de/ch1187/plugins4freva/climpact.git
    cd climpact
    conda env create -f conda-env.yml -n climpact

These commands would create a fresh conda environment named `climpact` after
activating the environment

::

    conda activate climpact

You can make use of the stand alone library.

"""
from . import _version
from .convert import *  # noqa: F401, F403
from .datacontainer import *  # noqa: F401, F403
from .run_directory import *  # noqa: F401, F403
from .swift import *  # noqa: F401, F403

__version__ = "2022.9.1"


__all__ = swift.__all__ + convert.__all__ + datacontainer.__all__ + run_directory.__all__  # type: ignore # noqa: F405


__version__ = _version.get_versions()["version"]
