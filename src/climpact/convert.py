# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

# import logging
import re
import warnings

import cf_xarray.units  # noqa: F401
import pandas as pd
import pint
import pint_xarray  # noqa: F401
import xarray as xr  # noqa: F401
from pint_xarray import unit_registry as ureg  # noqa: F401

__all__ = ["UnitConverter"]


class UnitConverter:
    UndefinedUnitError = pint.UndefinedUnitError
    DimensionalityError = pint.DimensionalityError

    units = pint.UnitRegistry(
        autoconvert_offset_to_baseunit=True, force_ndarray_like=True
    )

    # Capture v0.10 NEP 18 warning on first creation
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        units.Quantity([])
        # For pint 0.6, this is the best way to define a dimensionless unit. See pint #185
        units.define("percent = 1e-2 frac = pct")
        units.define("@alias percent = %")
        units.define("fraction = [] = frac")
        units.define(
            "@alias fraction = frac = frac. = fract. = fract = 1 = 1."
        )
        # Define commonly encountered units not defined by pint
        units.define(
            "degrees_north = degree = degrees_N = degreesN = degree_north = degree_N "
            "= degreeN"
        )
        units.define(
            "degrees_east = degree = degrees_E = degreesE = degree_east = degree_E "
            "= degreeE"
        )
        units.define("@alias h = hour")
        # Alias geopotential meters (gpm) to just meters
        units.define("@alias meter = metre = gpm")

        # Silence UnitStrippedWarning
    if hasattr(pint, "UnitStrippedWarning"):
        warnings.simplefilter("ignore", category=pint.UnitStrippedWarning)

    @classmethod
    def convert(cls, dset, outputunit):
        units = cls.units
        if not outputunit:
            return dset
        try:
            inputunit = dset.attrs["units"]
        except KeyError:
            warnings.warn("No input unit given, cannot convet")
            return dset
        if inputunit.lower() == outputunit.lower():
            return dset
        unit = [inputunit, outputunit]
        dt = pd.Timedelta(dset.time.diff(dim="time").values[0]).total_seconds()
        # Try to convert the units to pint.units objects
        for n, uu in enumerate(unit):
            if "frac" in unit[n] or unit[n] in ("1", "1."):
                # Non-units assignment
                uu = ""
            elif "%" in unit[n]:
                uu = "percent"
            else:
                # Pint cannot deal with units like s-1
                # try to identify those units and replace numeric values
                # with a '^' (if numeric characters are present but no
                # power sign is given - like in m s-1 -> m s^-1)
                for i in re.findall("[-\d]+", unit[n]):  # noqa: W605
                    if "^" not in unit[n] or "**" in unit[n]:
                        uu = uu.replace(i, f"^{i}")
            try:
                unit[n] = units(uu.replace("-^", "^-").replace("--", "-"))
            except Exception as e:
                print(
                    f"Unit conversion failed, input unit {inputunit}, "
                    f"target unit {outputunit}, unit passed to pint: {unit[n]}"
                )
                raise e
        inunit, outunit = unit
        quant = dset.pint.quantify()
        outputunit = str(units(outputunit).units)

        def _is_length_dim(inunit):
            return [dim[1:-1] for dim in inunit.units.dimensionality] == [
                "length"
            ]

        def tuple_pos(mul):
            for nn, k in enumerate(mul.dimensionality.keys()):
                if k == "[time]":
                    return nn

        if inunit == outunit:
            # If the units are euqal we do not need to do anything
            dset.attrs["units"] = outputunit
            return dset
        rho_w = 1000 * units("kg/m^3")  # ~ Density of water
        density_unit = units("kg/m**2").units
        if _is_length_dim(inunit) and outunit.units == density_unit:
            # Most likely this is a water based variable, hence convert
            # a water hight unit to a density unit using the density of water
            dset.attrs["units"] = outputunit
            return dset * (inunit.to_base_units() * rho_w).magnitude
        if _is_length_dim(outunit) and inunit.units == density_unit:
            # Same as above but vice versa
            conv = outunit.to_base_units() / rho_w
            dset.attrs["units"] = outputunit
            return dset * conv.to(inunit).magnitude
        mass_flux_unit = units("kg/m**2/s").units
        flux = (rho_w / (dt * units("s"))).to_base_units()
        speed_unit = units("meter/second").units
        if _is_length_dim(inunit) and outunit.units == mass_flux_unit:
            # Most likely this is a water based variable that needs to be
            # converted to flux unit by aplying rho_w and the time difference
            dset.attrs["units"] = outputunit
            return dset * (flux * inunit).to_base_units().magnitude
        if _is_length_dim(outunit) and inunit.units == mass_flux_unit:
            # Same flux case as above, just vice versa
            dset.attrs["units"] = outputunit
            return (
                dset * ((outunit.to_base_units() / flux).to(inunit)).magnitude
            )
        # Do a 'normal' magnitude conversion
        if inunit.units == density_unit and outunit.units == mass_flux_unit:
            # Input is density output mass flux -> divide by time unit
            dset.attrs["units"] = outputunit
            return dset * 1 / (dt * units("s")).to_base_units().magnitude
        if inunit.units == mass_flux_unit and outunit.units == density_unit:
            # Same as above but vice versa
            dset.attrs["units"] = outputunit
            return dset * (dt * units("s")).to_base_units().magnitude
        if (
            inunit.to_base_units().units == speed_unit
            and outunit.units == mass_flux_unit
        ):
            inunit = (inunit * rho_w).to_base_units()
        if (
            inunit.units == mass_flux_unit
            and outunit.to_base_units().units == speed_unit
        ):
            timeunit = units(outunit.to_tuple()[-1][tuple_pos(outunit)][0])
            outunit = (outunit * rho_w).to_base_units() * units("s") / timeunit
        conv = quant.pint.to(outunit.units).pint.dequantify()
        conv.attrs["units"] = outputunit
        return conv
