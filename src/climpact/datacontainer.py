# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

import json
import sys
from datetime import datetime
from getpass import getuser
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import List, Optional, Union, cast
from urllib.parse import ParseResult, urlparse
from zipfile import ZipFile

# import cftime
import fsspec
import pandas as pd
import requests
import xarray as xr
from IPython.display import HTML
from tqdm import tqdm

from .convert import UnitConverter
from .swift import Swift
from .utils import save_file

__all__ = ["DataContainer"]


class DataContainer:
    """
    Class that holds information about processed data.
    """

    def __init__(self, *datasets: tuple[xr.Dataset, ...]):
        self._datasets = {}
        self._attrs = {}
        self._path_prefix = ""
        now = datetime.now().strftime("%Y%M%d_%M%H%ST")
        for num, dset in enumerate(datasets):
            dset_ = cast(xr.Dataset, dset)
            model = (
                dset_.attrs["ensemble_member"] + "_" + dset_.attrs["region"]
            )
            self._datasets[model] = dset_
            self._attrs[model] = dset_.attrs
            if not self._path_prefix and "path_prefix" in dset_.attrs:
                self._path_prefix = (
                    f'PostProc_{dset_.attrs["path_prefix"]}_{now}'
                )

    @staticmethod
    def _repr_to_method(rep: str) -> str:
        """Convert str representation to method."""
        return (
            rep.strip(">")
            .strip("<class")
            .replace("'", "")
            .replace('"', "")
            .strip()
        )

    def __repr__(self):
        return self.container.__repr__()

    def __str__(self):
        return self.container.__str__()

    def _repr_html_(self):
        return self.container._repr_html_()

    def __iter__(self):
        return iter(self._datasets.values())

    @property
    def container(self):
        """
        Overview of the content of this data container.
        """

        df = pd.DataFrame({"model name": self._datasets.keys()})
        df.index.name = "index"
        return df

    @property
    def variables(self) -> List[str]:
        """Returns a list of all variables within the data container."""
        varnames = []
        for dset in self._datasets.values():
            for v in cast(dict, dset.data_vars):
                if "time" not in v and v not in varnames and "height" not in v:
                    varnames.append(v)
        return varnames

    def convert(self, variable: str, unit: str) -> None:
        """
        Convert all variables in the data container to a desired unit.

        ::

             dc = DataContainer.from_zipfile("output.zip")
             dc.convert("tas", "degC")

        Parameters
        ----------
        variable:
            the variable name that needs to be converted
        unit:
            the desired unit the variable is converted to
        """
        for model, dset in self._datasets.items():
            try:
                _ = dset[variable]
            except KeyError:
                raise KeyError(f"So such variable: {variable} in {model}")
            self._datasets[model][variable] = UnitConverter.convert(
                dset[variable], unit
            )

    def _save_data(
        self, dset: xr.Dataset, filename: Path, zpf: ZipFile
    ) -> None:
        attrs = dset.attrs
        df = dset.to_dataframe()
        for var in dset.data_vars:
            attrs[var] = dset[var].attrs
        attrs["time"] = dset.time.attrs
        if filename.suffix in (".h5", ".hdf5"):
            with pd.HDFStore(filename, "w") as store:
                store.put("data", df)
                store.get_storer("data").attrs.metadata = attrs
        else:
            with filename.with_suffix(".json").open("w") as f:
                json.dump(attrs, f)
            zpf.write(
                str(filename.with_suffix(".json")),
                filename.with_suffix(".json").name,
            )
            df.to_csv(filename)

    def _upload_to_swift(
        self,
        file_name: Path,
        swift_group: str,
        username: Optional[str],
        **kwargs,
    ) -> Optional[HTML]:
        try:
            swift = Swift(
                swift_group,
                username=username,
                password=kwargs.get("password", None),
            )
            url = (
                swift.upload(file_name.parent, "climpact")
                + f"/{file_name.name}"
            )
        except KeyboardInterrupt:
            pass
        if not self._is_kernel():
            print(f"Data was uploaded. To access the data use the url: {url}")
        else:
            import IPython

            return IPython.display.HTML(
                (
                    "<p>Data was uploaded. Access the data via the following link"
                    f"to the <a href={url} target=_blank>swift cloud storage.</a><p>"
                )
            )
        return None

    @staticmethod
    def _is_kernel():
        if "IPython" not in sys.modules:  # IPython hasn't been imported
            return False
        from IPython import get_ipython

        # check for `kernel` attribute on the IPython instance
        return getattr(get_ipython(), "kernel", None) is not None

    def save(
        self,
        filetype: str = "nc",
        username: Optional[str] = None,
        filename: Optional[Union[str, Path]] = None,
    ) -> None:
        """
        Save the dataset to a zip file and upload the zip file to swift cloud store.

        Parameters
        -----------
        filetype:
            The file extension the data is saved to, should be one of [h5, hdf5, nc, csv]
        username:
            The username that is used to login to the swift cloud store, if None is given (default),
            the current system user will be taken.
        filename:
            The desired file name of the saved zip file, if None is given (default)
            an informative filename will be constructed from the metadata.
        """
        allowed_extensions = ("h5", "hdf5", "nc", "nc4", "csv", "zarr")
        if filetype in "zarr":
            filename = (
                Path(filename or self._path_prefix).with_suffix(".zarr").name
            )
        else:
            filename = (
                Path(filename or self._path_prefix).with_suffix(".zip").name
            )
        filetype = filetype.strip(".")
        swift_group = None
        username = username or getuser()
        if filetype not in allowed_extensions:
            raise ValueError(
                f"Only the following file extensions ar supported : {allowed_extensions}"
            )
        with TemporaryDirectory() as td:
            zip_file = Path(td) / username / filename
            zip_file.parent.mkdir(exist_ok=True, parents=True)
            with save_file(zip_file, "w") as s_file:
                for dset in self._datasets.values():
                    fn = Path(dset["attrs"]["filename"]).with_suffix(
                        f".{filetype}"
                    )
                    if swift_group is None:
                        swift_group = dset.attrs["swift_group"]
                    out_f = Path(td) / fn
                    if filetype == "zarr":
                        dset.to_zarr(str(zip_file), mode="a", group=str(fn))
                    elif filetype == "nc":
                        dset.to_netcdf(out_f)
                        cast(ZipFile, s_file).write(str(out_f), out_f.name)
                    else:
                        self._save_data(dset, out_f, cast(ZipFile, zip_file))
                        cast(ZipFile, s_file).write(str(out_f), out_f.name)
            self._upload_to_swift(zip_file, str(swift_group), username)

    def replace(
        self,
        dset: xr.Dataset,
        index: Optional[int] = None,
        name: Optional[str] = None,
    ) -> None:
        """
        Replace a dataset in a container

        Parameters
        -----------
        dset:
            The dataset that needs to be replaced
        index:
            The index of the dataset in the container that is replaced
        name:
            The name of the model member of the according dataset that is replaced
        """
        if name is not None:
            try:
                self._datasets[name] = dset
            except KeyError:
                raise KeyError(
                    f"No such model member {name}, check members with '.container' property"
                )
        elif index is not None:
            try:
                key = list(self._datasets.keys())[index]
            except IndexError:
                raise IndexError(
                    f"No model member for index {index}, check indices with '.container' property"
                )
            self._datasets[key] = dset
        else:
            raise ValueError(
                "Select datasets either by setting the 'by_index' or 'by_name' mthod."
            )

    def select(
        self, *, by_index: Optional[int] = None, by_name: Optional[str] = None
    ) -> xr.Dataset:
        """
        Select a dataset from the container.

        ::

            dc = DataContainer.from_zipfile("output.zip")
            dc.container
            dataset1 = dc.select(by_index=0)
            dataset2 = dc.select(by_name="foo_bar")

        Parameters
        ----------
        by_index:
            select the dataset based on the index in the container.
        by_name:
            select the dataset based on the model name in the container.

        Returns
        -------
        Returns xarray dataset

        """
        if by_name is not None:
            try:
                return self._datasets[by_name]
            except KeyError:
                raise KeyError(
                    (
                        f"No such model member {by_name}, check members"
                        "with '.container' property"
                    )
                )
        if by_index is not None:
            try:
                key = list(self._datasets.keys())[by_index]
            except IndexError:
                raise IndexError(
                    f"No model member for index {by_index}, check indices with '.container' property"
                )
            return self._datasets[key]
        raise ValueError(
            "Select datasets either by setting the 'by_index' or 'by_name' mthod."
        )

    @classmethod
    def _download_file(cls, url: ParseResult, tmp_dir: Path) -> Path:
        """Download a file form a given url."""

        file_name = tmp_dir / Path(url.path).name
        req = requests.get(url.geturl(), stream=True)
        fileSize = int(req.headers["Content-Length"])
        chunkSize = 4096
        with file_name.open("wb") as f:
            with tqdm(
                unit="B",
                unit_scale=True,
                unit_divisor=1024,
                miniters=1,
                desc="Downloading",
                total=fileSize,
                leave=True,
            ) as pbar:
                for chunk in req.iter_content(chunk_size=chunkSize):
                    f.write(chunk)
                    pbar.update(len(chunk))
        return file_name

    @classmethod
    def _read_zarr(cls, source: Union[str, Path]) -> List[xr.Dataset]:
        """Read datasets from a zarr storage."""
        datasets = []
        if str(source).startswith("http"):
            fs = fsspec.filesystem("swift")
        else:
            fs = fsspec.filesystem("file")
        for type_dict in fs.listdir(source):
            name = Path(type_dict["name"]).name
            if type_dict["type"] == "directory":
                datasets.append(
                    xr.open_zarr(source, group=name, use_cftime=True)
                )
        if len(datasets) == 0:
            datasets.append(xr.open_zarr(source, use_cftime=True))
        return datasets

    @classmethod
    def _read_zipfile(
        cls, z_file: Union[Path, ParseResult]
    ) -> List[xr.Dataset]:
        """Read the content of a zipfile."""
        datasets = []
        with TemporaryDirectory() as td, TemporaryDirectory() as zip_d:
            if isinstance(z_file, ParseResult):
                zipfile = cls._download_file(z_file, Path(zip_d))
            else:
                zipfile = z_file
            tdd = Path(td)
            with ZipFile(str(zipfile)) as zf:
                files = map(Path, zf.namelist())
                zf.extractall(td)
                for file in files:
                    if file.suffix in (".nc", ".nc4"):
                        datasets.append(xr.open_dataset(tdd / file))
                        continue
                    elif file.suffix == ".csv":
                        json_file = file.with_suffix(".json")
                        with (tdd / json_file).open() as f:
                            attrs = json.load(f)
                        data = pd.read_csv(tdd / file).to_xarray()
                    elif file.suffix in (".hdf5", ".h5"):
                        with pd.HDFStore(tdd / file, "r") as f:
                            data = f.get("data").to_xarray()
                            attrs = f.get_storer("data").attrs.metadata
                    else:
                        continue
                    for var in data.data_vars:
                        try:
                            data[var].attrs = attrs.pop(var)
                        except KeyError:
                            pass
                    data.attrs = attrs
                    try:
                        Time = pd.DatetimeIndex(data["time"].values)
                    except Exception:
                        Time = []
                        method = eval(
                            cls._repr_to_method(data["time"].attrs["dtype"])
                        )
                        for time in data["time"].values:
                            date, t = time.split(" ")
                            args_date = tuple(map(int, date.split("-")))
                            args_t = tuple(map(int, t.split(":")))
                            args = args_date + args_t
                            Time.append(method(*args))
                    Time = xr.DataArray(
                        Time,
                        attrs=data.time.attrs,
                        dims=("index",),
                        coords={"index": Time},
                        name="time",
                    )
                    dataset = xr.Dataset()
                    dataset.attrs = data.attrs
                    for dv in data.data_vars:
                        if dv != "time":
                            dataset[dv] = data[dv]
                    dataset = dataset.assign_coords({"index": Time})
                    dataset["index"].attrs = Time.attrs
                    datasets.append(
                        dataset.rename_dims({"index": "time"}).rename(
                            {"index": "time"}
                        )
                    )
        return datasets

    @classmethod
    def from_zipfile(cls, *zipfile: Union[str, Path]):
        """
        Create a DataContainer from a zipfile that contians the time series data.

        ::

            dc1 = DataContainer.from_zipfile("output.zip")
            dataset1 = dc1.select(by_index=0)
            dc2 = DataContainer.from_zipfile("*")
            dataset2 = dc2.select(by_index=0)

        Parameters
        ----------
        zipfile:
            Path to the zip file that contains the saved data, glob pattern like '*' or '*.zip' are also
            excepted.

        Returns
        -------
        Returns The DataContainer ojbect holding all nessecary information to post-process the time-series data

        """
        zip_files, zarr_files = [], []
        for z_file in zipfile:
            if str(z_file).startswith("http"):
                parsed_file = urlparse(str(z_file))
                if Path(parsed_file.path).suffix == ".zarr":
                    zarr_files.append(str(z_file))
                else:
                    zip_files.append(parsed_file)
            elif Path(z_file).expanduser().absolute().is_file():
                parsed_file = urlparse(str(z_file))
                if Path(parsed_file.path).suffix == ".zip":
                    zip_files.append(parsed_file)
                else:
                    zarr_files.append(str(z_file))
            elif Path(z_file).expanduser().is_dir():
                zip_files += [
                    urlparse(str(f))
                    for f in Path(z_file).expanduser().rglob("*.zip")
                ]
                zarr_files += [
                    str(f) for f in Path(z_file).expanduser().rglob("*.zarr")
                ]
            elif Path(z_file).expanduser().parent.is_dir():
                tf = Path(z_file).expanduser().absolute()
                if tf.suffix == ".zarr":
                    zarr_files += [str(f) for f in tf.parent.rglob(tf.name)]
                else:
                    zip_files += [
                        urlparse(str(f)) for f in tf.parent.rglob(tf.name)
                    ]
            else:
                if Path(z_file).suffix == ".zip":
                    zip_files += [
                        urlparse(str(f)) for f in Path(".").rglob(str(z_file))
                    ]
                else:
                    zarr_files += [
                        str(f) for f in Path(".").rglob(str(z_file))
                    ]

        datasets = []
        for dataset in [cls._read_zipfile(zf) for zf in zip_files]:
            datasets += dataset
        for dataset in [cls._read_zarr(zf) for zf in zarr_files]:
            datasets += dataset
        return cls(*datasets)  # type: ignore
