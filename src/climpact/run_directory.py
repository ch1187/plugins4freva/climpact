# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

""":py:mod:`RunDirectory` is a module of the climpact package that can be used to
open climate datasets and select region within this dataset. The definitions
of the regions are based on polygons defined in shape files. See also the
:ref:`Api Reference <api>` section for more details.
"""

import json
import logging
import warnings
from datetime import timedelta
from functools import cached_property
from itertools import product
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Any, Dict, List, Mapping, Optional, Tuple, Union, cast
from zipfile import ZipFile

import cartopy.feature as cfeature
import cftime

# import fiona
# import fsspec
import geopandas as gp
import matplotlib
import numpy as np
import pandas as pd
import pyproj
import regionmask
import xarray as xr
from cartopy import crs
from cartopy.feature import ShapelyFeature
from cartopy.mpl.geoaxes import GeoAxes
from dask import array as da
from dask.array import Array as dastarray
from matplotlib import pyplot as plt

from .utils import get_datetime, open_mfdataset

matplotlib.use("Agg")


__all__ = ["RunDirectory", "Averager", "get_datetime", "open_mfdataset"]


class Averager:
    """Collection of methods to create averages."""

    def __init__(
        self,
        shapefile: str,
        region: str,
        mask: Optional[Tuple[str, str]] = None,
    ):
        if mask is None:
            self.mask_file = self.mask_type = None
        else:
            self.mask_file = Path(mask[0])
            self.mask_type = mask[-1]
        self.region = region
        self.shapefile = shapefile or None

    def get_mask(
        self,
        dset: xr.DataArray,
        method: str = "centres",
        shape_data: Optional[gp.geopandas.GeoDataFrame] = None,
    ) -> xr.DataArray:
        """Create a mask region from the shape file information."""
        dims = dset.dims[-2:]
        lat, lon = dset[dims[0]].values, dset[dims[-1]].values
        lat_idx, lon_idx = np.arange(lat.shape[0]), np.arange(lon.shape[0])
        coords = {
            "lat_idx": cast(np.ndarray, lat_idx),
            "lon_idx": cast(np.ndarray, lon_idx),
        }
        dims = ("lat_idx", "lon_idx")
        if shape_data is None:
            data = da.from_array(np.ones((lat.shape[0], lon.shape[0])))
            X, Y = np.meshgrid(lon, lat)
            mask_data_array = xr.DataArray(data, coords=coords, dims=dims)
            mask_lat = xr.DataArray(X, dims=dims, coords=coords)
            mask_lon = xr.DataArray(Y, dims=dims, coords=coords)
            mask_dataset = xr.Dataset(
                {"mask": mask_data_array, "lat": mask_lat, "lon": mask_lon}
            )
            mask = mask_dataset.set_coords(("lat", "lon"))["mask"]
            return mask
        if method == "centres":
            mask = regionmask.mask_geopandas(
                shape_data, lon, lat, method="shapely"
            )
            mask.data = (mask.data * 0) + 1
        else:
            dlat, dlon = np.diff(lat), np.diff(lon)
            dlat = np.r_[dlat[0], dlat] / 2.0
            dlon = np.r_[lon[0], dlon] / 2.0
            corners_y = (lat - dlat, lat + dlat)
            corners_x = (lon - dlon, lon + dlon)
            list_masks = []
            temp_mask = None
            for i, j in product(range(2), range(2)):
                c_lat, c_lon = corners_y[i], corners_x[j]
                m = regionmask.mask_geopandas(
                    self.shape_data,  # type: ignore
                    c_lon,
                    c_lat,
                    method="shapely",
                )
                m.data = (m.data * 0) + 1
                if temp_mask is None:
                    temp_mask = m
                list_masks.append(m.data)
            masks = np.array(list_masks)
            if temp_mask is None:
                raise ValueError("Could not create mask")
            dims = ("corners",) + temp_mask.dims
            coords = {
                "corners": cast(np.ndarray, np.array(range(len(masks)))),
                **cast(Dict[str, np.ndarray], temp_mask.coords),
            }
            mask = xr.DataArray(
                masks, dims=dims, coords=coords, name=temp_mask.name
            ).max(dim="corners")
        mask.data = da.from_array(mask.data)
        dims = dset.dims[-2:]
        mask_dims = mask.dims
        mask = mask.rename({mask_dims[i]: dims[i] for i in range(len(dims))})
        return mask

    @cached_property
    def georefdata(self):
        """Read the geo reference data."""
        if not isinstance(self.shapefile, (str, Path)):
            return None
        try:
            shape = gp.read_file(str(self.shapefile))
        except Exception as e:
            logging.error("Shape file could not be read")
            raise e
        return shape

    def get_native_shape_data(self, region):
        """Read the given shape-file."""
        if self.georefdata is None:
            return None

        geo_key = [
            key for key in self.georefdata.columns if "geometry" in key.lower()
        ]
        if not geo_key:
            logging.warning("Shape file does not contain 'geometry' key")
            return
        # Transform the shape file projection to the data projection
        if not region:
            return self.georefdata[geo_key[0]]
        try:
            shape_re = self.georefdata.loc[
                self.georefdata.index == int(region)
            ]
        except ValueError:
            keys = [k for k in self.georefdata.columns if k != geo_key[0]]
            shape_re = []
            for index in self.georefdata.index:
                values = self.georefdata[keys].loc[index]
                if region in list(values.values):
                    break
            shape_re = self.georefdata.loc[self.georefdata.index == index]
        if len(shape_re) > 0:
            return shape_re[geo_key[0]]
        return self.georefdata[geo_key[0]]

    def get_extent(
        self,
        dset: xr.DataArray,
        shape_data: Optional[gp.geodataframe.GeoDataFrame] = None,
    ) -> Dict[str, slice]:
        """Calculate the lon/lat bounds of the valid data within a dataset."""
        lat_name, lon_name = map(str, dset.dims[-2:])
        dlon = np.diff(dset[lon_name].data)[0]
        dlat = np.diff(dset[lat_name].data)[0]
        if shape_data is None:
            return {}
        return {
            lon_name: slice(
                shape_data.bounds["minx"].min() - dlon,
                shape_data.bounds["maxx"].max() + dlon,
            ),
            lat_name: slice(
                shape_data.bounds["miny"].min() - dlat,
                shape_data.bounds["maxy"].max() + dlat,
            ),
        }

    @staticmethod
    def _weighted(data, avg_dims, mask, skipna, *args, **kwargs):
        """Calculate weight average with a given mask."""

        latn = avg_dims[0]
        lonn = avg_dims[1]
        weight = Averager._get_area(data[latn].data, data[lonn].data) * mask
        return (data * weight).sum(
            avg_dims, skipna=skipna, keep_attrs=True
        ) / np.nansum(weight)

    @staticmethod
    def seasonal_mean(ds: xr.Dataset, skipna: bool = False) -> xr.Dataset:
        month_length = ds.time.dt.days_in_month
        # Calculate the weights by grouping by 'time.season'.
        weights = (
            month_length.groupby("time.season")
            / month_length.groupby("time.season").sum()
        )
        # Test that the sum of the weights for each season is 1.0
        np.testing.assert_allclose(
            weights.groupby("time.season").sum().values, np.ones(4)
        )
        # Calculate the weighted average
        return (
            (ds * weights)
            .groupby("time.season")
            .sum(dim="time", skipna=skipna, keep_attrs=True)
        )

    @staticmethod
    def _get_area(lat, lon):
        """Calculate the area of a gird-cell."""

        R = 6371.0  # Earth's radius in km
        dlon = np.deg2rad(np.fabs(lon[0] - lon[1]))
        areas = (
            np.pi
            / 360
            * R**2
            * np.fabs(np.diff(np.sin(np.deg2rad(lat))))
            * dlon
        )
        center_lat = lat[1:] - np.fabs(lat[0] - lat[1]) / 2
        areas = np.interp(lat, center_lat, areas)
        return np.meshgrid(np.ones_like(lon), areas)[-1]

    @staticmethod
    def fldmean(data, dims=("lat", "lon"), land_frac=None, skipna=True):
        """
        CDO equivalent of field mean.

        Parameters
        ----------
            data : xr.DataArray, xr.Dataset
                input data
            dims: tuple, default: (lat, lon)
                geographical dimensions names
            land_frac: xr.DataArray, dask.array, numpy.array, default: None
                mask that is applied to the data
            skipna: bool, default: True
                drop nan values
        Returns
        -------
            xr.DataArray, xr.Dataset: field mean
        """

        if land_frac is not None:
            try:
                mask = np.ma.masked_invalid(land_frac.values)
            except AttributeError:
                mask = np.ma.masked_invalid(land_frac)
            mask = da.from_array(mask.round(0))
        else:
            mask = 1
        avg = Averager._weighted(data, dims, mask, skipna)
        for var in data.data_vars:
            avg[var].attrs = data[var].attrs
        return avg


class RunDirectory(Averager):
    """The :py:class:`RunDirectory` class is a class for reading data.

    It offers easy control over selecting complex regions defined in
    geo-reference datasets like shape or geojson files. See also the
    :ref:`Api Reference <api>` section for more details.


    Parameters
    ----------

    files: list[str]
        List of files that should be opened by the reader
    variables: list[str]
        list variables that should be considered
    shape_file: str, default: None
        Path to the shape file that is used to define the mask region. If None
        given (default) no masking will be applied.
    region: str, default: ""
        Select the name of a sub region within the shape file. If None given
        (default) the whole geometry defined in the shape file is taken.
    mask_method: str, default: centres
        String representing the method how the masked region (if given) should
        be applied. The string can either be *centres* or *corners*. Where
        *centres* selects only those grid-boxes that have the grid-box centres
        within the coordinates of the mask region. *corners* will select
        grid-boxes that have at least one of the four corners of the grid-boxes
        within the coordinates of the mask region. This means that *corners*,
        compared to *centres* will slightly increase the selected area.
    mask: tuple[str, int], default: None
        If additionally a land or sea region should be mask this variable can be
        used to set the path to a land-sea mask file (first entry in the tuple)
        and the type of the mask (second entry - 0: land, 1: sea).
    abort_on_error: bool, default: False
        Exit if something goes wrong while loading the dataset
    reindex: bool, default: True
        Apply nearest neighbor re-indexing to a larger grid in order to achieve
        a more precise region selection.
    kwargs:
        Additional information about the run

    Examples
    --------

    First we read a dataset without applying a mask at all

    ::

        from climpact import RunDirectory
        rd = RundDirectory(["~/orog.nc"], ["orog"])
        print(rd.variables)
        ['orog']
        print(type(rd.dataset))
        <class 'xarray.core.dataset.Dataset'>
        print(rd.dataset["orog"])
        <xarray.DataArray 'orog' (rlat: 412, rlon: 424)>
        dask.array<mul, shape=(412, 424), dtype=float64, chunksize=(412, 424), chunktype=numpy.ndarray>
        Coordinates:
            lon      (rlat, rlon) float64 dask.array<chunksize=(412, 424), meta=np.ndarray>
            lat      (rlat, rlon) float64 dask.array<chunksize=(412, 424), meta=np.ndarray>
          * rlon     (rlon) float64 -28.38 -28.26 -28.16 -28.05 ... 17.93 18.05 18.16
          * rlat     (rlat) float64 -23.38 -23.26 -23.16 -23.05 ... 21.61 21.73 21.83
            Y        (rlat, rlon) float64 21.99 22.03 22.07 22.11 ... 66.81 66.75 66.69
            X        (rlat, rlon) float64 -10.06 -9.964 -9.864 ... 64.55 64.76 64.96
        Attributes:
            standard_name:  surface_altitude
            long_name:      Surface Altitude
            units:          m
            grid_mapping:   rotated_pole


    Now let's apply a mask defined in a shape file

    ::

        from climpact import RunDirectory
        rd = RundDirectory(["~/orog.nc"], ["orog"], shape_file="Germany.shp")
        print(rd.dataset["orog"])
        <xarray.DataArray 'orog' (rlat: 365, rlon: 275)>
        dask.array<mul, shape=(365, 275), dtype=float64, chunksize=(365, 275), chunktype=numpy.ndarray>
        Coordinates:
          * rlon     (rlon) float64 -7.695 -7.673 -7.652 -7.63 ... -1.798 -1.777 -1.755
          * rlat     (rlat) float64 -3.245 -3.223 -3.201 -3.18 ... 4.631 4.653 4.675
            lon      (rlat, rlon) float64 dask.array<chunksize=(365, 275), meta=np.ndarray>
            lat      (rlat, rlon) float64 dask.array<chunksize=(365, 275), meta=np.ndarray>
            Y        (rlat, rlon) float64 46.92 46.92 46.92 46.93 ... 55.39 55.39 55.39
            X        (rlat, rlon) float64 6.713 6.745 6.776 6.807 ... 14.84 14.88 14.92
        Attributes:
            standard_name:  surface_altitude
            long_name:      Surface Altitude
            units:          m
            grid_mapping:   rotated_pole


    Finally we can only select a sub region in the shape file by giving the key
    or index to the sub region.

    ::

        from climpact import RunDirectory
        rd = RundDirectory(["~/orog.nc"],
                           ["orog"],
                           shape_file="Germany.shp",
                           region="Schaumburg",
                           )
        print(rd.dataset["orog"])
        <xarray.DataArray 'orog' (rlat: 25, rlon: 25)>
        dask.array<mul, shape=(25, 25), dtype=float64, chunksize=(25, 25), chunktype=numpy.ndarray>
        Coordinates:
          * rlon     (rlon) float64 -5.605 -5.587 -5.568 -5.55 ... -5.202 -5.183 -5.165
          * rlat     (rlat) float64 1.595 1.613 1.632 1.65 ... 1.98 1.998 2.017 2.035
            lon      (rlat, rlon) float64 dask.array<chunksize=(25, 25), meta=np.ndarray>
            lat      (rlat, rlon) float64 dask.array<chunksize=(25, 25), meta=np.ndarray>
            Y        (rlat, rlon) float64 52.0 52.0 52.0 52.01 ... 52.48 52.49 52.49
            X        (rlat, rlon) float64 8.876 8.905 8.935 8.964 ... 9.444 9.474 9.504
        Attributes:
            standard_name:  surface_altitude
            long_name:      Surface Altitude
            units:          m
            grid_mapping:   rotated_pole
    """

    earth_radius = 6370000

    def __init__(
        self,
        files: List[Union[str, Path]],
        variables: List[str],
        abort_on_error: bool = False,
        mask: Optional[Tuple[str, str]] = None,
        reindex: bool = True,
        parallel: bool = True,
        **kwargs,
    ) -> None:
        self.transform = crs.PlateCarree()
        self.proj = crs.PlateCarree()
        self._reindex = reindex
        super().__init__(
            kwargs.get("shape_file", ""), kwargs.get("region", ""), mask
        )
        self.files = sorted(files)
        self._variables = sorted(map(str.lower, variables))
        self.metadata = kwargs.copy()
        self.mask_method = self.metadata.pop("mask_method", "centres")
        self.abort_on_error = abort_on_error
        self._open_parallel = parallel

    @cached_property
    def variables(self) -> list[str]:
        """The variables as they are stored in the dataset."""
        if self._dataset is None:
            return []
        return [
            cast(str, var)
            for var in self._dataset.data_vars
            if (cast(str, var) in self._variables)
            or (cast(str, var).lower() in self._variables)
        ]

    @cached_property
    def shape_data(self) -> Optional[gp.GeoDataFrame]:
        """Shape data in the projection of the netcdf data."""
        shape_data = self.get_native_shape_data(self.region)
        if shape_data is None:
            return None
        return shape_data.to_crs(self.proj)

    @cached_property
    def _lsm(self) -> dastarray:
        """Land-sea mask"""

        dset_dims = cast(xr.Dataset, self._dataset)[self.variables[0]].dims[
            -2:
        ]
        if self.mask_file is None or self.mask_type is None:
            dims = cast(xr.Dataset, self._dataset).dims
            return da.from_array(
                np.ones((dims[dset_dims[0]], dims[dset_dims[1]]))
            )
        mask = xr.open_dataset(self.mask_file)["sftlf"].load()
        mul: Union[float, int] = 1
        if mask.attrs["units"].lower() in ("%", "prec", "percent"):
            mul = 0.01
        dims = mask.dims  # type: ignore
        new_coords = cast(xr.Dataset, self._dataset)[self.variables[0]].coords
        try:
            target = {d: new_coords[d] for d in dims}
        except KeyError:
            logging.warning("Could not remap land-sea mask to target grid.")
            return da.from_array(
                np.ones((dims[dset_dims[0]], dims[dset_dims[1]]))
            )
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            new_mask = mask.interp(coords=target, method="linear")
        if self.mask_type == "land":
            data = np.where(new_mask.values * mul < 0.5, 1, np.nan)
        else:
            data = np.where(new_mask.values * mul >= 0.5, 1, np.nan)
        return da.from_array(data)

    @staticmethod
    def _get_time_slice(
        dset: xr.Dataset, start: pd.Timestamp, end: pd.Timestamp
    ) -> xr.Dataset:
        """Select a time slice of a dataset for given time period."""
        (s, e), dtype = get_datetime(dset.time, [start, end])
        s = cast(cftime.DatetimeNoLeap, s)
        units, calendar = "days since 1970-01-01", s.calendar
        nums = slice(*cftime.date2num([s, e], units, calendar=calendar))
        time = dset.time.values
        dd = dset.assign_coords(
            {"time": cftime.date2num(time, units, calendar)}
        )
        dd = dd.sel(time=nums)
        dd = dd.assign_coords(
            {"time": cftime.num2date(dd.time.values, units, calendar)}
        )
        attrs = dd["time"].attrs
        attrs["dtype"] = dtype
        dd["time"].attrs = attrs

        return dd

    def _sel_time(self, dset: xr.Dataset) -> xr.Dataset:
        try:
            time_vec = dset.time.values  # noqa: F841
        except AttributeError:
            # No time given
            return dset
        if not self.metadata.get("start", None):
            start = dset.time.values[0]
        else:
            start = pd.Timestamp(self.metadata["start"])
        if not self.metadata.get("end", None):
            end = dset.time.values[-1]
        else:
            end = pd.Timestamp(self.metadata["end"])
        try:
            return dset.sel(time=slice(start, end))
        except Exception as e:  # noqa: F841
            return self._get_time_slice(dset, start, end)

    def _set_projection(self, dataset: xr.Dataset) -> None:
        proj = [
            p
            for p in ("rotated_pole", "rotated_latitude_longitude")
            if p in dataset.data_vars
        ]
        if not proj:
            dataset.attrs["proj"] = "4326"

        else:
            dataset.attrs["proj"] = "rotated_pole"
            rot_pole = dataset[proj[0]].attrs
            for key, value in rot_pole.items():
                dataset.attrs[key] = value
            dataset.attrs["proj"] = "rotated_pole"
            pole = (
                rot_pole["grid_north_pole_longitude"],
                rot_pole["grid_north_pole_latitude"],
            )
            dataset.attrs["grid_north_pole_longitude"] = pole[0]
            dataset.attrs["grid_north_pole_latitude"] = pole[1]
            self.proj = crs.RotatedPole(
                pole_longitude=pole[0],
                pole_latitude=pole[-1],
            )
        dataset.attrs["projection"] = self.proj.__str__()
        dataset.attrs["transform"] = self.transform.__str__()
        if self.shape_data is None:
            dataset.attrs["shape_projection"] = ""
        else:
            dataset.attrs["shape_projection"] = crs.Projection(
                self.get_native_shape_data(self.region).crs
            ).__str__()

    def _get_X(self, dataset: xr.Dataset) -> xr.DataArray:
        """Eastward coords. in projection of input shapefile."""
        dims = dataset[self.variables[0]].dims[-2:]
        coords = {d: dataset[d] for d in dims}
        if self.get_native_shape_data(self.region) is None:
            proj = crs.PlateCarree()
        else:
            proj = crs.Projection(self.get_native_shape_data(self.region).crs)
        if proj.is_geographic:
            units = "degrees_east"
        else:
            units = "m"
        X_attr = dict(
            standard_name="X",
            long_name="Eastward coordinates in projection of shapefile",
            units=units,
            axis="X",
        )
        Y, X = np.meshgrid(
            dataset[dims[0]], cast(xr.Dataset, self._dataset)[dims[1]]
        )
        # Convert the data and create xarray datasets
        X_new, _, _ = proj.transform_points(self.proj, X, Y).T
        return xr.DataArray(
            X_new, name="X", dims=dims, coords=coords, attrs=X_attr
        )

    def _get_Y(self, dataset: xr.Dataset) -> xr.DataArray:
        """Northward coords. in projection of input shapefile."""
        dims = dataset[self.variables[0]].dims[-2:]
        coords = {d: dataset[d] for d in dims}
        if self.get_native_shape_data(self.region) is None:
            proj = crs.PlateCarree()
        else:
            proj = crs.Projection(self.get_native_shape_data(self.region).crs)
        if proj.is_geographic:
            units = "degrees_north"
        else:
            units = "m"
        Y_attr = dict(
            standard_name="Y",
            long_name="Northward coordinates in projection of shapefile",
            units=units,
            axis="Y",
        )
        Y, X = np.meshgrid(
            dataset[dims[0]], cast(xr.Dataset, self._dataset)[dims[1]]
        )
        # Convert the data and create xarray datasets
        _, Y_new, _ = proj.transform_points(self.proj, X, Y).T
        return xr.DataArray(
            Y_new, name="Y", dims=dims, coords=coords, attrs=Y_attr
        )

    def set_shape_projection_coords(self, dataset: xr.Dataset) -> xr.Dataset:
        """Add coordinates in the projection of the given input shapefile.

        Parameters
        ----------
         datasset:
             The input dataset the new coordinates are added to

        Returns
        -------
        xarray.Dataset:
            Xarray dataset where coordiantes in eastward dirction (X) and
            northward direction (Y) have been added a coordinates

        """
        dataset["Y"] = self._get_Y(dataset)
        dataset["X"] = self._get_X(dataset)
        return dataset.set_coords(("Y", "X"))

    @property
    def bounding_box(self) -> Dict[str, slice]:
        """Get the max boundingbox of non NaN values."""
        return self.get_extent(
            cast(xr.Dataset, self.dataset)[self.variables[0]], self.shape_data
        )

    @cached_property
    def mask(self) -> xr.DataArray:
        """Create a gridded mask that is calculated from the shape region."""
        return self.get_mask(
            cast(xr.Dataset, self.dataset)[self.variables[0]],
            self.mask_method,
            shape_data=self.shape_data,
        )

    def plot_maps(
        self,
        dset: xr.Dataset,
        fig_prefix: str,
        shape_data: Optional[gp.geodataframe.GeoDataFrame] = None,
    ) -> None:
        """
        Plot map data.

        Parameters
        -----------
        max_index:
            The number of timesteps to be considered why plotting the map.
        fig_prefix:
            Prefix of the file name that is plotted
        shape_data:
            the pol borders that should be plotted.
        """
        try:
            dt = (
                dset.time - (dset.time.isel(time=0) + timedelta(hours=24))
            ).values
            idx = np.argmin(np.fabs(dt.astype(np.int64)))
        except Exception:
            idx = min(dset.time.shape[0], 24)
        for varname in set(self.variables):
            plot_data = (
                dset[varname].isel(time=slice(0, idx)).mean(dim="time").load()
            )
            plot_data.attrs = dset[varname].attrs
            title = f"Sanity check for {varname} - {self.metadata.get('model', 'no model')} ({self.metadata.get('exp', 'no exp')})"
            self._plot_map(
                plot_data,
                title,
                f"{fig_prefix}_{varname}.png",
                shape_data=shape_data,
            )

    @cached_property
    def regionborder(self) -> ShapelyFeature:
        """The region border polygon as a cartopy feature."""
        return self._get_regionborder(self.shape_data)

    def _get_regionborder(self, shape_data):
        try:
            shape_feature = ShapelyFeature(
                shape_data.to_crs(epsg=4326),
                self.transform,
                edgecolor="black",
                facecolor="never",
            )
        except AttributeError:
            shape_feature = cfeature.BORDERS.with_scale("10m")
        return shape_feature

    def _to_cartopy_proj(self, proj: pyproj.crs.crs.CRS) -> crs.Projection:
        code = int(proj.to_string().split(":")[-1])
        try:
            return crs.epsg(code)
        except ValueError:
            pass
        import pyepsg
        from cartopy._epsg import _GLOBE_PARAMS

        projection = pyepsg.get(code)
        proj4_str = projection.as_proj4().strip()
        terms = [term.strip("+").split("=") for term in proj4_str.split(" ")]
        globe_terms = filter(lambda term: term[0] in _GLOBE_PARAMS, terms)
        globe = crs.Globe(
            **{_GLOBE_PARAMS[name]: value for name, value in globe_terms}
        )
        return crs.PlateCarree(globe=globe)

    def _plot_map(
        self,
        dset: xr.DataArray,
        title: str,
        fname: str,
        shape_data: Optional[gp.geopandas.GeoDataFrame] = None,
    ) -> None:
        """
        Plot the data on a map.

        Parameters
        ----------
        varname:
            The variable to be plotted
        title:
            Figure sub title
        fname:
            File name of the plot
        """
        plt.rc("font", size=14)
        xy_ratio = dset.shape[1] / dset.shape[0]
        if xy_ratio <= 1:
            cbar_orientation = "vertical"
            figsize = (12, 12 * xy_ratio)
            bottom: Union[float, int] = 0
            right = 0.75
        else:
            cbar_orientation = "horizontal"
            figsize = (12, 12 / xy_ratio)
            bottom = 0.1
            right = 1

        dims = dset.dims[-2:]
        lon = dset[dims[-2]].values.ravel()  # noqa: F841
        lat = dset[dims[-1]].values.ravel()  # noqa: F841
        if shape_data is None:
            shape_data = self.shape_data
        # Increase the map extent by 5% but not more than 1 deg
        try:
            # Try getting the plot extent from the mask region
            ext = [
                cast(gp.geopandas, shape_data).bounds["minx"].min(),
                cast(gp.geopandas, shape_data).bounds["maxx"].max(),
                cast(gp.geopandas, shape_data).bounds["miny"].min(),
                cast(gp.geopandas, shape_data).bounds["maxy"].max(),
            ]
        except AttributeError:
            ext = []
        cbar_label = f'{dset.attrs["long_name"]} ' f'[{dset.attrs["units"]}]'
        fig = plt.figure(figsize=figsize)
        ax = cast(GeoAxes, fig.add_subplot(111, projection=self.proj))
        _ = dset.plot(  # type: ignore
            ax=ax,
            transform=self.proj,
            add_colorbar=True,
            cbar_kwargs={
                "label": cbar_label,
                "extend": "both",
                "aspect": 20,
                "orientation": cbar_orientation,
            },
        )
        ax.set_title("")
        # position bottom right
        fig.text(
            0.95,
            0.05,
            "Sanity check: not for publication.",
            fontsize=30,
            color="gray",
            ha="right",
            va="bottom",
            alpha=0.5,
        )
        for feature in (
            cfeature.COASTLINE.with_scale("10m"),
            self._get_regionborder(shape_data),
        ):
            ax.add_feature(feature)
        if ext:
            _ = ax.set_xlim(ext[:2]), ax.set_ylim(ext[-2:])
        fig.subplots_adjust(
            left=0, right=right, hspace=0, wspace=0, top=0.85, bottom=bottom
        )
        fig.suptitle(title, x=0.1, ha="left")
        fig.tight_layout()
        fig.savefig(fname, dpi=150, bbox_inches="tight", format="png")
        fig.clf()
        plt.close()

    @staticmethod
    def save_table(dset: xr.Dataset, filename: Path) -> None:
        """
        Convert xr.Dataset to pandas dataframe.

        Parameters
        ----------
        dset:
            xarray input dataset
        filename:
            output file path
        """
        attrs = dset.attrs
        d_vars = list(dset.data_vars)
        multi_dim = any([len(dset[v].dims) > 1 for v in d_vars])
        for var in dset.data_vars:
            attrs[var] = dset[var].attrs
        attrs["time"] = dset.time.attrs
        if filename.suffix in (".h5", ".hdf5"):
            if multi_dim:
                dset.to_netcdf(filename, format="NETCDF4")
            else:
                df = dset[d_vars].to_dataframe()
                with pd.HDFStore(filename, "w") as store:
                    store.put("data", df)
                    store.get_storer("data").attrs.metadata = attrs
        else:
            df = dset[d_vars].to_dataframe()
            if multi_dim:
                warnings.warn(
                    "You are attempting to store multi dimensional "
                    "arrays in a text format, which is a bad idea. "
                    "Choose either a different format or make sure "
                    "that your data was pre-processed properly "
                )
            with filename.with_suffix(".json").open("w") as f:
                json.dump(attrs, f)
            df.to_csv(filename)

    @classmethod
    def save_data(
        cls, file: Union[ZipFile, str], dset: xr.Dataset, filetype: str = "nc"
    ):
        """
        Save the time series data to disk

        Parameters
        ----------
        file:
            The zipfile object where the data is saved to
        dset:
            the data that should be saved to file
        filetype:
            file type of the output data
        """
        attrs = dset.attrs
        if filetype == "zarr":
            fn = attrs["filename"]
        else:
            fn = f"{attrs['filename']}.{filetype}"
        for key, values in attrs.items():
            attrs[key] = str(values)
        for var in dset.data_vars:
            for key, values in dset[var].attrs.items():
                dset[var].attrs[key] = str(values)
        with TemporaryDirectory() as td:
            out_f = Path(td) / fn
            if filetype == "nc":
                dset.to_netcdf(out_f)
            elif filetype == "zarr":
                dset.to_zarr(cast(str, file), group=fn, mode="a")
                return
            else:
                cls.save_table(dset, out_f)
                if filetype == "csv":
                    attr_file = out_f.with_suffix(".json")
                    cast(ZipFile, file).write(str(attr_file), attr_file.name)
            cast(ZipFile, file).write(str(out_f), fn)

    @cached_property
    def _dataset(self) -> Optional[xr.Dataset]:
        try:
            dd = open_mfdataset(
                self.files,
                variables=self._variables,
                parallel=self._open_parallel,
            )
        except Exception as e:
            if self.abort_on_error:
                raise e
            mdata = f'{self.metadata.get("model","")} / {self.metadata.get("exp", "")}'  # noqa: E231
            logging.warning(f"Too many files are corrupted for {mdata}")
            dd = None
        if dd is None:
            return None
        dset = self._sel_time(dd)
        variables = [
            cast(str, var)
            for var in dset.data_vars
            if cast(str, var).lower() in self._variables
        ]
        self._variables = (
            variables  # databrowser is lowercase! do not want that!
        )
        self._set_projection(dset)
        mask = self.get_mask(  # noqa: F841
            dset[variables[0]], self.mask_method, self.shape_data
        )
        extent = self.get_extent(dset[variables[0]], self.shape_data)
        dset = dset.sel(**cast(Mapping[Any, Any], extent))
        new_coords = {}
        if extent and self._reindex:
            for d in dset[variables[0]].dims[-2:]:
                new_coords[str(d)] = np.linspace(
                    dset[d].values[0], dset[d].values[-1], dset[d].size * 5
                )
            dset = dset.reindex(**new_coords, method="nearest")
        return dset

    @cached_property
    def dataset(self) -> Optional[xr.Dataset]:
        """The selected sub region multiplied by a land-see mask (if chosen)."""
        if self._dataset is None:
            mdata = f'{self.metadata.get("model","")} / {self.metadata.get("exp", "")}'  # noqa: E231
            logging.error(f"Too many files are corrupted for {mdata}")
            return None
        for var in self._variables:
            try:
                self._dataset[var].data *= self._lsm
            except ValueError:
                pass
        return self.set_shape_projection_coords(self._dataset)
