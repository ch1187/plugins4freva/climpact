# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

"""Module that handles swift conections."""

import base64
import json
import logging
import multiprocessing as mp
import os
import time
from datetime import datetime
from functools import partial
from getpass import getpass
from pathlib import Path
from typing import Optional, Union, cast
from urllib.parse import urljoin

import appdirs
import requests
from swiftclient.multithreading import OutputManager
from swiftclient.service import SwiftError, SwiftService, SwiftUploadObject

BASE_URL = "https://swift.dkrz.de/"
AUTH_VERS = 1.0
SWIFT_AUTH_TYPE = Optional[Union[str, bool, float, int, list[str]]]
"""Type for arguments to connect to swift storage url."""
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("swiftclient").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

__all__ = ["Swift"]


def _getUploadObject(
    inp_dir: Union[str, Path],
    path: Union[str, Path],
    options: Optional[dict[str, bool]] = None,
) -> SwiftUploadObject:
    """Helper function to create an upload object from a path."""
    if options:
        sw_upload = partial(SwiftUploadObject, options=options)
    else:
        sw_upload = SwiftUploadObject
    swift_file = Path(path).relative_to(Path(inp_dir).parent)
    return sw_upload(str(path), str(swift_file))


def _print_status(res, container, **kwargs) -> None:
    """Print status of swift a operation."""
    if res["success"]:
        if "object" in res:
            logger.info(res["object"])
        elif "for_object" in res:
            logger.info(
                "%s segment %s" % (res["for_object"], res["segment_index"])
            )
    else:
        error = res["error"]
        if res["action"] == "create_container":
            logger.warning(
                "Warning: failed to create container " "'%s'%s",
                container,
                error,
            )
        elif res["action"] == "upload_object":
            logger.error(
                "Failed to upload object %s to container %s: %s"
                % (container, res["object"], error)
            )
        else:
            logger.error("%s" % error)


class Swift:
    """Estabilish a connection to a dkrz swift cloud store.

    Parameters
    ----------
    account: str
        Account name that is used as swift login
    username: str, optional
        Username to logon if not given (default) set to account name. Which
        is equivalent to logging on to the personal siwft.
    password: str, optional
        Password used to logon to the swift store. If not given (default)
        and a password is needed a password prompt will ask for the password.

        .. note:: A password will only be needed if no login authorisation
                  token exists or the token has expired.

    Attributes
    ----------
    auth_token: str
        Swift authentication token.
    """

    def __init__(
        self,
        account: str,
        username: Optional[str] = None,
        password: Optional[str] = None,
    ):
        """Determine if the swift auth-token exists and is still valid."""
        _swift_status = self._get_swift_token_status(account)
        self.account = account
        username = username or self.account
        now = datetime.now()
        _expires: float = cast(float, _swift_status["expires"])
        try:
            expires_in = (
                datetime.fromtimestamp(_expires) - now
            ).total_seconds()
        except TypeError:
            expires_in = -1

        if _swift_status["auth_token"] is None or expires_in < 1:
            self._get_auth(username, password)
        elif password:
            self._get_auth(username, password)
        else:
            self.auth_token = cast(str, _swift_status["auth_token"])
            self.storage_url = cast(str, _swift_status["storage_url"])
            self.expires = _swift_status["expires"]
        self._write_token_to_disk()

    @staticmethod
    def get_passwd(
        msg: str = "User password for cloud storage", wait_sec: int = 60
    ) -> str:
        """
        Get a password from the getpass prompt

        Parameters
        ----------
        msg:
            user defined message to be displayed when asking for the password.
        wait_sec:
            amount of seconds to wait before terminating the password request.

        Returns
        -------
            the password, or None if terminated.
        """
        if not wait_sec:
            return getpass(f"{msg}: ")

        def gpass(queue: mp.Queue, msg: str) -> None:
            queue.put(getpass(msg))

        queue: mp.Queue = mp.Queue()
        proc = mp.Process(
            target=gpass,
            args=(
                queue,
                f"{msg}: ",
            ),
        )
        proc.start()
        n_itt = 0
        while proc.is_alive():
            time.sleep(0.1)
            if n_itt > wait_sec * 10:
                proc.terminate()
                raise RuntimeError("Time out during password retrieval.")
            n_itt += 1
        password = queue.get()
        proc.join()
        return password

    def _get_auth(self, username, password) -> None:
        """Connect to the swift store to query the authentication token."""
        username = os.environ.get("CI_SWIFT_USER", username)
        password = os.environ.get("CI_SWIFT_PASSWD", password)
        self.account = os.environ.get("CI_SWIFT_ACCOUNT", self.account)
        headers = {
            "X-Auth-User": f"{self.account}:{username}",  # noqa: E231
            "X-Auth-Key": f"{password or self.get_passwd()}",  # noqa: E231
        }
        url = urljoin(BASE_URL, f"auth/v{AUTH_VERS}")
        resp = requests.get(url, headers=headers)
        try:
            resp.raise_for_status()
        except requests.RequestException:
            raise requests.RequestException(f"Connection to {url} failed")
        self.auth_token = resp.headers["x-auth-token"]
        expires = int(resp.headers["x-auth-token-expires"])
        self.storage_url = resp.headers["x-storage-url"]
        self.expires = time.time() + expires

    @property
    def swift_file(self) -> Path:
        """File where the information to the swift connection is stored."""
        return Path(appdirs.user_data_dir()) / "swift.conf"

    def _write_token_to_disk(self) -> None:
        try:
            content = json.loads(self._get_token_file_content())
        except json.JSONDecodeError:
            content = {}
        update = {
            self.account: dict(
                auth_token=self.auth_token,
                storage_url=self.storage_url,
                expires=self.expires,
            )
        }
        content.update(update)
        self.swift_file.parent.mkdir(exist_ok=True, parents=True)
        with self.swift_file.open("bw") as f_obj:
            f_string = json.dumps(content)
            f_obj.write(base64.b64encode(f_string.encode()))
        self.swift_file.chmod(0o600)

    def _get_token_file_content(self) -> str:
        """Read the content of the token file."""
        try:
            with self.swift_file.open("r") as f_obj:
                return base64.b64decode(f_obj.read().encode()).decode()
        except FileNotFoundError:
            return "{}"

    def _get_swift_token_status(
        self, container: str
    ) -> dict[str, Optional[Union[str, float]]]:
        """Try reading the token from the disk."""

        try:
            login_info = json.loads(self._get_token_file_content())[container]
        except (json.JSONDecodeError, KeyError):
            login_info = {}
        _swift_status: dict[str, Optional[Union[str, float]]] = dict()
        for key in "auth_token", "storage_url", "expires":
            try:
                _swift_status[key] = login_info[key]
            except KeyError:
                _swift_status[key] = None
        return _swift_status

    @property
    def _connect_options(
        self,
    ) -> dict[str, Union[SWIFT_AUTH_TYPE, dict[str, SWIFT_AUTH_TYPE]]]:
        return {
            "help": False,
            "os_help": False,
            "snet": False,
            "verbose": 1,
            "debug": False,
            "info": False,
            "auth": None,
            "auth_version": "2.0",
            "user": " ",
            "key": " ",
            "retries": 5,
            "insecure": False,
            "ssl_compression": True,
            "force_auth_retry": False,
            "prompt": False,
            "os_username": " ",
            "os_user_id": None,
            "os_user_domain_id": None,
            "os_user_domain_name": None,
            "os_password": " ",
            "os_tenant_id": None,
            "os_tenant_name": None,
            "os_project_id": None,
            "os_project_name": None,
            "os_project_domain_id": None,
            "os_project_domain_name": None,
            "os_auth_url": None,
            "os_auth_type": None,
            "os_application_credential_id": None,
            "os_application_credential_secret": None,
            "os_auth_token": self.auth_token,
            "os_storage_url": self.storage_url,
            "os_region_name": None,
            "os_service_type": None,
            "os_endpoint_type": None,
            "os_cacert": None,
            "os_cert": None,
            "os_key": None,
            "changed": False,
            "skip_identical": False,
            "segment_size": None,
            "segment_container": None,
            "leave_segments": False,
            "object_threads": 20,
            "segment_threads": 20,
            "meta": [],
            "header": [],
            "use_slo": False,
            "object_name": None,
            "checksum": True,
            "os_options": {
                "user_id": None,
                "user_domain_id": None,
                "user_domain_name": None,
                "tenant_id": None,
                "tenant_name": None,
                "project_id": None,
                "project_name": None,
                "project_domain_id": None,
                "project_domain_name": None,
                "service_type": None,
                "endpoint_type": None,
                "auth_token": self.auth_token,
                "object_storage_url": self.storage_url,
                "region_name": None,
                "auth_type": None,
                "application_credential_id": None,
                "application_credential_secret": None,
            },
            "object_uu_threads": 20,
        }

    def _get_filenames(self, inp: Path) -> list[SwiftUploadObject]:
        """Walk through a given directory and return all filenames."""

        inp = inp.expanduser().absolute()
        if inp.is_file():
            return [SwiftUploadObject(str(inp), inp.name)]
        objs = []
        dir_markers = []
        opts = {"dir_marker": True}
        for _dir, _ds, _fs in os.walk(inp):
            if not (_ds + _fs):
                dir_markers.append(_getUploadObject(inp, _dir, options=opts))
            else:
                objs.extend(
                    [
                        _getUploadObject(inp, os.path.join(_dir, _f))
                        for _f in _fs
                    ]
                )
        return objs + dir_markers

    def upload(self, inp_dir: Union[str, Path], container: str) -> str:
        """
        Upload a folder to a given swift-container.

        Parameters:
        ===========
            inp_dir: str, pathlib.Path
                Source input folder that is going to be uploaded
            container: str
                Name of the swift container
        """
        inp_dir = Path(inp_dir)
        parent = inp_dir.name
        with SwiftService(options=self._connect_options) as swift:
            with OutputManager():
                objs = self._get_filenames(inp_dir)
                try:
                    for r in swift.upload(container, objs):
                        _print_status(r, container)
                except SwiftError as e:
                    logger.error(e.value)
            status = dict(swift.stat(container)["items"])
            read_acl = ".r:*,.rlistings"
            if status["Read ACL"] != read_acl:
                swift.post(container, options={"read_acl": ".r:*,.rlistings"})
        public_url = urljoin(BASE_URL, f"v{int(AUTH_VERS)}")
        container_url = (
            f'{public_url}/{status["Account"]}/{container}/{parent}'
        )
        return container_url


if __name__ == "__main__":
    import sys

    print("Username: ", end="")
    username = input() or None
    print("Account: ", end="")
    account = input() or username
    connection = Swift(str(account), username)
    inp_dir = Path(sys.argv[1]).expanduser().absolute()
    connection.upload(inp_dir, inp_dir.name)
