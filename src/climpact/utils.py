# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

"""Collection of utility methods and classes."""
import zipfile
from contextlib import contextmanager
from datetime import datetime, timezone
from pathlib import Path
from typing import Any, Iterator, List, Literal, Optional, Tuple, Union, cast

import dask
import numpy as np
import pandas as pd
import xarray as xr


@contextmanager
def save_file(
    inp_file: Union[Path, str], mode: Literal["r", "w", "x", "a"] = "w"
) -> Iterator[Union[zipfile.ZipFile, str]]:
    inp_file = Path(inp_file)
    out_file: Union[zipfile.ZipFile, str]
    if inp_file.suffix == ".zarr":
        out_file = str(inp_file)
    else:
        out_file = zipfile.ZipFile(str(inp_file), mode=mode)
    try:
        yield out_file
    finally:
        if isinstance(out_file, zipfile.ZipFile):
            out_file.close()


def open_mfdataset(
    inp_files: List[Union[str, Path]],
    thresh: float = 0.1,
    parallel: bool = True,
    variables: Optional[List[str]] = None,
) -> xr.Dataset:
    """Open multiple datasets.

    This is a re-implementation of the xarray.mf_dataset method.
    The only difference is that is assumes a list of files paths, glob pattern
    won't be working. Additionally a threshold for the fraction files that
    can be currupeted when trying to open datasets can be passed.


    Parameters
    ----------
    inp_files: list[str]
        List of filepaths to be opened.
    thresh: float, default: 0.1
        Fraction of the number of files that can be corrupted, if the fraction
        is surpassed and error is raised.
    parallel: bool, default: True
        Open all files in parallel.
    variables: list, default: None
        A list of variable names that have to be present in the combined
        dataset, if any variables are missing an error is raised. By
        default (None) no checks for the presence of variables is performed.


    Raises
    ------
    ValueError: if the max fraction of corrupted files is surpassed or not
                all variables are present in the merged dataset.

    """

    def _open_dataset(in_file: str | Path, **kwargs) -> xr.Dataset:
        if Path(in_file).suffix in [".nc", ".nc4", ".h5", ".hdf5", "cdf"]:
            kwargs["engine"] = "h5netcdf"
        try:
            return xr.open_dataset(in_file, **kwargs)
        except Exception as error:
            return xr.Dataset(attrs={"_mf_open_error": error})

    num_failed = 0
    errors: list[str] = []
    if parallel:
        _open = dask.delayed(_open_dataset)
        _getattr = dask.delayed(getattr)
    else:
        _open = _open_dataset
        _getattr = getattr
    datasets = [
        _open(p, chunks={"time": 24}, use_cftime=True) for p in inp_files
    ]
    closers = [_getattr(ds, "_close") for ds in datasets]
    if parallel:
        datasets, closer = dask.compute(datasets, closers)

    for dataset in datasets:
        error = dataset.attrs.get("_mf_open_error", "")
        if error:
            num_failed += 1
            errors.append(error)

    if num_failed / len(inp_files) > thresh:
        error_str = "\n".join(errors)
        msg = f"Too many errors {num_failed}\n\n{error_str}"
        for ds in datasets:
            ds.close()
        raise ValueError(msg)
    combined = xr.combine_by_coords(
        datasets,
        compat="override",
        coords="minimal",
        data_vars="minimal",
        combine_attrs="override",
    )
    combined = cast(xr.Dataset, combined)

    def multi_file_closer():
        for closer in closers:
            closer()

    combined.set_close(multi_file_closer)
    data_vars = list(map(str, combined.data_vars))
    data_vars = list(map(str.lower, data_vars))
    variables = variables or data_vars
    variables = list(map(str.lower, variables))
    if len(set(variables) & set(data_vars)) != len(
        variables
    ):  # all vars in databrowser are lowercase!!
        # Not all variables we needed are present
        combined.close()
        raise ValueError("Could not fetch all data variables")
    return combined


def get_datetime(
    da: Union[np.ndarray, xr.DataArray],
    time: Union[List[datetime], np.ndarray],
    **kwargs: Union[str, bool],
) -> Tuple[List[Union[Any, datetime]], str]:
    """
    Match a list time timestamps to the time object in a given data array.

    Parameters
    -----------
    da : numpy.array
        time array as xarray.DataArray that contains the target time object
    time: collection
        collection of datetime objects that will be matched
    kwargs:
        Additional keyword arguments that can be used to overwrite,
        certain timestamp attributes e.g. year=2020
    Returns
    -------
    list: list of timestamps with of the same type as da

    """
    try:
        da = pd.DatetimeIndex(da)
        typ = pd.Teimestamp
    except Exception:
        typ = np.asarray(da)[0]
        if isinstance(typ, np.datetime64):
            time = [
                datetime.fromtimestamp(t, tz=timezone.utc)
                for t in cast(np.ndarray, time).astype("O") / 1e9
            ]
            typ = datetime
        else:
            typ = type(typ)
    add_kwargs = {}
    try:
        add_kwargs["has_year_zero"] = typ.has_year_zero
    except AttributeError:
        pass
    return [
        typ(
            kwargs.get("year", None) or t.year,
            kwargs.get("month", None) or t.month,
            kwargs.get("day", None) or t.day,
            kwargs.get("hour", None) or t.hour,
            kwargs.get("minute", None) or t.minute,
            kwargs.get("second", None) or t.second,
            **add_kwargs,
        )
        for t in time
    ], repr(typ)
