# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

import pytest


def update_evaluation_system_config():
    import configparser

    evaluation_system_config_file = "plugin_env/freva/evaluation_system.conf"
    config = configparser.ConfigParser()
    config.read(evaluation_system_config_file)
    config.sections()
    config["evaluation_system"]["db.host"] = "127.0.0.1"
    config["evaluation_system"]["db.user"] = "freva"
    config["evaluation_system"]["db.passwd"] = "T3st"
    config["evaluation_system"]["db.db"] = "freva"
    config["evaluation_system"]["db.port"] = "3306"
    config["evaluation_system"]["solr.host"] = "localhost"
    with open(evaluation_system_config_file, "w") as f:
        config.write(f)


def update_drs_config():
    import toml

    drs_config_file = "plugin_env/freva/drs_config.toml"
    config = toml.load(drs_config_file)
    # TODO: Add your custom DRS configuration here
    config["nukleus-kit"] = {
        "root_path": "/home/freva/work/ch1187/freva-regiklim/data/model/regional/nukleus-kit",
        "drs_format": "cordex",
    }
    config["nukleus-kit.defaults"] = {"project": "nukleus"}

    with open(drs_config_file, "w") as file:
        toml.dump(config, file)


@pytest.fixture(scope="session", autouse=True)
def update_freva_configs():
    try:
        update_evaluation_system_config()
        update_drs_config()
        print("Configuration updated successfully.")
    except Exception as e:
        raise e
