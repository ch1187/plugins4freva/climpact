# SPDX-FileCopyrightText: 2024 Deutsche Klimarechenzentrum
#
# SPDX-License-Identifier: BSD-3-Clause

"""Test file for imports."""
# TODO: we need to improve this test file to work for the plugin


def test_package_import():
    """Test the import of the main package."""
    print("Testing import of the main package.")
    # import climpact  # noqa: F401
